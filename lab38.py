# การสร้างหน้าต่างย่อยๆ 

def donothing():
    print('donothing')
    #messagebox.showinfo(title='lab38',message='version 1.0 2022-10-07')
    messagebox.showerror('Error', 'Error message')
    messagebox.showwarning('Warning','Warning message')
    messagebox.showinfo('Information','Informative message')

def exitapp():

   res = messagebox.askquestion('Exit Application', 'Do you really want to exit')
   if res == 'yes':
     app.destroy()
   else :
     messagebox.showinfo('Return', 'Returning to main application')


def openreport():
    print('open report')
     
    #rp = tk.Tk() # ถ้าท่านี้จะเปิดหลาย winodws ได้!!
    rp = tk.Toplevel(app) # ป้องกันการเปิด windows หลายอัน
    rp.title('report dialog')
    button = tk.Button(rp,text='view report')
    button.place(x=10,y=10)
    
    
    # ป้องกันการเปิด windows หลายอัน เอกสารประกอบ : https://stackoverflow.com/questions/29233029/python-tkinter-show-only-one-copy-of-window
    rp.transient(app) 
    rp.grab_set()
    app.wait_window(rp)

    rp.geometry('200x100')
    


import tkinter as tk 
from tkinter import messagebox

app = tk.Tk()
app.title('แบบฝึกหัดของชั้น')

button = tk.Button(app,text='view report', command=openreport)
button.place(x=10,y=10)

# การสร้างเมนูและเมนูย่อย
mymenu = tk.Menu(app)

filemenu = tk.Menu(mymenu, tearoff=0)
filemenu.add_command(label='New',command=donothing)
filemenu.add_command(label='Open',command=donothing)
filemenu.add_command(label='Exit',command=exitapp)
mymenu.add_cascade(label='File', menu=filemenu)

aboutmenu = tk.Menu(mymenu, tearoff=0)
aboutmenu.add_command(label='About',command=donothing)
mymenu.add_cascade(label='Help', menu=aboutmenu)

app.geometry('500x500')
app.config(menu=mymenu)
app.mainloop()
