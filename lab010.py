def print_something():
    print('Hi..')
    for i in coll_hobby:
        for k in i:
            print(k,i[k].get())
 
import tkinter as tk
from tkinter import Checkbutton

from tkinter import IntVar 

app = tk.Tk()
app.title('Hello world')

#เพิ่มปุ่มเข้ามา
label1 = tk.Label(app,text='งานอดิเรกของคุณ : ',width=12)
#label1.place(x=0,y=0)
label1.grid(row=0,column=0,sticky='e')
 
v1 = IntVar(app)
Checkbutton1 = Checkbutton(app, text="ดูหนัง",variable = v1,
                           onvalue=1, offvalue=0).grid(row=1,column=1,sticky='w',)

v2 = IntVar(app)
Checkbutton2 = Checkbutton(app, text="เล่นกีฬา",variable = v2,
                           onvalue=1, offvalue=0).grid(row=2,column=1,sticky='w')

v3 = IntVar(app)
Checkbutton3 = Checkbutton(app, text="ปลูกต้นไม้",variable = v3,
                           onvalue=1, offvalue=0).grid(row=3,column=1,sticky='w')


coll_hobby = [{'ดูหนัง':v1},{'เล่นกีฬา':v2},{'ปลูกต้นไม้':v3}] #เก็บค่า ที่เปลี่ยนเมื่อ checkbox เปลี่ยน



button = tk.Button(app,text='click me!',bg = "yellow", command=print_something)
#button.place(x=50,y=0) #replace values with required
#button.pack()  #comment this
button.grid(row=4,column=1,sticky='w')

app.geometry("300x300")
app.mainloop()