def print_something(k):
    print('Button bind working!' +str(k))


import tkinter as tk
from functools import partial

app = tk.Tk()
app.title('Hello world')

#เพิ่มปุ่มเข้ามา
button = tk.Button(text='click me!', command=partial(print_something,5))
button.place(x=100,y=300) #replace values with required
#button.pack()  #comment this

app.geometry("500x500")
app.mainloop()