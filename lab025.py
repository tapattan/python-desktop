from tkinter.simpledialog import askinteger,askfloat,askstring
import tkinter as tk


def print_something():
    #prompt = askinteger("Input", "Input an Integer")
    #prompt = askfloat("Input", "Input an Float")
    prompt = askstring("Input", "Input an String")
    
    print(prompt)


app = tk.Tk()
app.title('Hello world')

#เพิ่มปุ่มเข้ามา
button = tk.Button(text='click me!', command=print_something)
button.pack()


app.geometry("300x300")
app.mainloop()