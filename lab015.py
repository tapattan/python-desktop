def print_something():
    print('Hi..'+text.get(1.0, 'end-1c'))
 
    
import tkinter as tk
from tkinter import Text,END
from tkinter import Scrollbar

app = tk.Tk()
app.resizable(True, True)
app.title("Scrollbar Widget Example")

 
# create the text widget
text = Text(app,width=40,height=10 , highlightthickness=1, borderwidth=2, relief="groove")
text.grid(row=0, column=0, sticky='e')

for i in range(20):
 text.insert(END, 'This is a cat '+str(i)+'\n')

# create a scrollbar widget and set its command to the text widget
scrollbar = Scrollbar(app, orient='vertical', command=text.yview)
scrollbar.grid(row=0, column=1, sticky='ns')

#  communicate back to the scrollbar
text['yscrollcommand'] = scrollbar.set


button = tk.Button(app,text='click me!',bg = "yellow", command=print_something)
#button.place(x=50,y=0) #replace values with required
#button.pack()  #comment this
button.grid(row=2,column=0,sticky='w')


app.geometry("300x300")
app.mainloop()