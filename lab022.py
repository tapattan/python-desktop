from tkinter import Tk, Frame, Canvas, TkVersion
from PIL import Image, ImageTk

print(TkVersion)

t = Tk()
t.title("Transparency")

frame = Frame(t)
frame.pack()

canvas = Canvas(frame,bg='yellow', width=300, height=300,bd=0,highlightthickness=0)
canvas.pack()

photoimage = ImageTk.PhotoImage(file="assets/sun.png")
canvas.create_image(150, 150, image=photoimage)

t.config(bg='yellow')

t.mainloop()