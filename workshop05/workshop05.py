from PIL import Image, ImageTk
from itertools import count, cycle
import threading
import random
import tkinter as tk
from tkinter import Label
from functools import partial
from PIL import Image, ImageTk
#pip3 install tkmacosx
import time
from tkmacosx import Button,Radiobutton
 
class ImageLabel(tk.Label):
    """
    A Label that displays images, and plays them if they are gifs
    :im: A PIL Image instance or a string filename
    """
    def load(self, im,resize=0):
        if isinstance(im, str):
            im = Image.open(im)
            if(resize!=0):
              im = im.resize((100,100))
        frames = []
 
        try:
            for i in count(1):
                frames.append(ImageTk.PhotoImage(im.copy()))
                im.seek(i)
        except EOFError:
            pass
        self.frames = cycle(frames)
 
        try:
            self.delay = im.info['duration']
        except:
            self.delay = 100
 
        if len(frames) == 1:
            self.config(image=next(self.frames))
        else:
            self.next_frame()
 
    def unload(self):
        self.config(image=None)
        self.frames = None
 
    def next_frame(self):
        if self.frames:
            self.config(image=next(self.frames))
            self.after(self.delay, self.next_frame)


def actionStart():
    LbDisplayResult.configure(text='')
    th = threading.Thread(target=botRandom)
    th.start()
    

def userSelect():
    if(var1.get()==1):
        f1.place(x=5,y=5)

    elif(var1.get()==2):
        f1.place(x=145,y=5)
    
    elif(var1.get()==3):
        f1.place(x=285,y=5)

def botRandom():
    imgRot.unload()
    imgRot.load('3-2-1-2.gif')
    time.sleep(3)
    imgRot.unload()
    
    k = random.randint(0,2)
    imgRot.load(listItemName[k],100)
    
    k = k+1
    user = var1.get()
    if(user==k):
        LbDisplayResult.configure(text='คุณเสมอ')

    if(user==1 and k==2):
        LbDisplayResult.configure(text='คุณชนะ')
    if(user==1 and k==3):
        LbDisplayResult.configure(text='คุณแพ้') 

    if(user==2 and k==1):
        LbDisplayResult.configure(text='คุณแพ้')
    if(user==2 and k==3):
        LbDisplayResult.configure(text='คุณชนะ')

    if(user==3 and k==1):
        LbDisplayResult.configure(text='คุณชนะ')
    if(user==3 and k==2):
        LbDisplayResult.configure(text='คุณแพ้')  


app = tk.Tk()
app.title('แบบฝึกหัด : เกมของชั้น')

f1 = tk.Label(text='',bg='red')
f1.place(x=5,y=5,width=115,height=115)

var1 = tk.IntVar(value=1)

################# zone rock scissors paper ##########
load= Image.open('rock.png').convert('RGBA')
load = load.resize((100, 100))
renderRock = ImageTk.PhotoImage(load)
imgRock = Radiobutton(app,image=renderRock,indicatoron=False,value=1, variable=var1 ,command=partial(userSelect))
imgRock.place(x=10,y=10)

load= Image.open('scissors.png').convert('RGBA')
load = load.resize((100, 100))
renderScissors = ImageTk.PhotoImage(load)
imgRock = tk.Radiobutton(app,image=renderScissors,indicatoron=False,value=2, variable=var1 ,command=partial(userSelect))
imgRock.place(x=150,y=10)

load= Image.open('paper.png').convert('RGBA')
load = load.resize((100, 100))
renderPaper = ImageTk.PhotoImage(load)
imgRock = tk.Radiobutton(app,image=renderPaper,indicatoron=False,value=3, variable=var1 ,command=partial(userSelect))
imgRock.place(x=290,y=10)
######### end zone 

# list item
listItem = [renderRock,renderScissors,renderPaper]
listItemName = ['rock.png','scissors.png','paper.png']

button = Button(text='Start!',command=partial(actionStart))
button.place(x=160,y=130)  

imgRot = ImageLabel(app)
imgRot.place(x=150,y=180)

LbDisplayResult = tk.Label(text='',width=10,font=("Arial", 40))
LbDisplayResult.place(x=90,y=300)

app.geometry("410x400")
app.mainloop()

