def print_something(k):
    print('Hi..' +str(k))
  

#pip install tksheet 

import tkinter as tk
from functools import partial
from tksheet import Sheet
#https://www.activestate.com/resources/quick-reads/how-to-display-data-in-a-table-using-tkinter/
#https://github.com/ragardner/tksheet/wiki#13-row-heights-and-column-widths

app = tk.Tk()
app.title('Hello world')

#เพิ่มปุ่มเข้ามา
button = tk.Button(app,text='click me!', command=partial(print_something,'David'))
button.place(x=0,y=0) #replace values with required
 
data = [] 
for row in range(5):#row
    _ = []
    for col in range(4):
      _.append('data'+str(row)+','+str(col)) 
    data.append(_) 

#[[f"Row {r}, Column {c}\nnewline1\nnewline2" for c in range(4)] for r in range(5)])
sheet = Sheet(app,column_width = 80,width=380,height=200,show_y_scrollbar = True,show_x_scrollbar = False,
               data = data)
 
 
sheet.enable_bindings()
sheet.place(x=20,y=50)

app.geometry("500x300")
app.mainloop()
