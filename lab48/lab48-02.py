# Python program to create a table
  
from tkinter import *
import tkinter as tk
from tkinter import Radiobutton,IntVar
from functools import partial

import pandas as pd 

def print_something():
    if(var.get() == 1):
        pass
    elif(var.get() == 2):
        pass

def show_frame(cont):
        if(cont==1):
          C1.tkraise()
        if(cont==2):
          C2.tkraise() ##โชว์ C2
          
        
          #for child in scrollable_frame.winfo_children():
          #    child.destroy()

          ############ part load data to table ############
          container01_table = tk.Frame(C2)
          canvas = tk.Canvas(container01_table)
          scrollbar = tk.Scrollbar(container01_table, orient="vertical", command=canvas.yview)
          scrollable_frame = tk.Frame(canvas)

          scrollable_frame.bind(
              "<Configure>",
              lambda e: canvas.configure(
                  scrollregion=canvas.bbox("all"),
                  height=200,
                  width=450,
              )
          )

          canvas.create_window((0, 0), window=scrollable_frame, anchor="nw")
          canvas.configure(yscrollcommand=scrollbar.set)

          df = pd.read_csv('member.csv')
          lst = []
          for i in range(len(df['no'])):
            row = (df.iloc[i]['no'],df.iloc[i]['item'],df.iloc[i]['type'],df.iloc[i]['amount'])
            lst.append(row)
            
          # find total number of rows and
          # columns in list
          total_rows = len(lst)
          total_columns = len(lst[0])

          t = Table(scrollable_frame,lst,total_rows,total_columns)

          container01_table.place(x=10,y=100)
          canvas.pack(side="left", fill="both", expand=True)
          scrollbar.pack(side="right", fill="y")
          ############ end part load data to table ############
          
      
          
           

          k = df[['amount','type']].groupby('type')['amount'].sum()
          k[0],k[1],k[1]-k[0]
          label6.configure(text=str(k[1]))
          label7.configure(text=str(k[0]))
          label9.configure(text=str(k[1]-k[0]))

          



def save_info(k):
    print(E1.get(),E2.get(),var.get())
    item = E1.get()
    amount = E2.get()
    type = var.get()
    

    df = pd.read_csv('member.csv')
    lst = []
    for i in range(len(df['no'])):
      row = (df.iloc[i]['no'],df.iloc[i]['item'],df.iloc[i]['type'],df.iloc[i]['amount'])
      lst.append(row)

    no = len(lst)+1
    lst.append((no,item,type,amount))
   

    no = []
    item = []
    type = []
    amount = []

    for i in lst:
      no.append(i[0])
      item.append(i[1])
      type.append(i[2])
      amount.append(i[3])


    df = pd.DataFrame({'no':no,'item':item,'type':type,'amount':amount})
    df.to_csv('member.csv')

     
class Table:
     
    def __init__(self,root,lst,total_rows,total_columns):
        # code for creating table
      
        for i in range(total_rows):
            for j in range(total_columns):
                if(j==0): #กำหนดให้ 0 คือ columns แรก
                  w_ = 3
                elif(j==1):
                  w_ = 50
                else:
                  w_ = 7
                 
                self.e = Entry(root, width=w_, fg='black',
                               font=('Arial',10,'bold'),)
                 
                self.e.grid(row=i, column=j)
                self.e.insert(END, lst[i][j])
                print('call table',lst[i][j]) 

                

app = tk.Tk()
app.title('Test')

C1 = tk.Frame(app,height=500,width=500,bg='black') 
label1 = tk.Label(C1,text='บันทึกรายการ',bd=5,width=10,bg='white')
label1.place(x=10,y=10)
button1 = tk.Button(C1,text='บันทึกรายการ',bd=5,command=partial(show_frame,1))
button1.place(x=10,y=50)
button2 = tk.Button(C1,text='สรุปผล',bd=5,command=partial(show_frame,2))
button2.place(x=120,y=50)
C1.place(x=0,y=0)

C2 = tk.Frame(app,height=500,width=500,bg='black') 
label1 = tk.Label(C2,text='สรุปผล',bd=5,width=10,bg='white')
label1.place(x=10,y=10)
button1 = tk.Button(C2,text='บันทึกรายการ',bd=5,command=partial(show_frame,1))
button1.place(x=10,y=50)
button2 = tk.Button(C2,text='สรุปผล',bd=5,command=partial(show_frame,2))
button2.place(x=120,y=50)
C2.place(x=0,y=0)

label2 = tk.Label(C1,text='ประเภท',bd=6,width=5,bg='white')
label2.place(x=10,y=100)
label3 = tk.Label(C1,text='รายการ',bd=6,width=5,bg='white')
label3.place(x=10,y=130)
button3 = tk.Button(C1,text='Click me to save!',bd=5,command=partial(save_info,0))
button3.place(x=100,y=180)

label4 = tk.Label(C2,text='รายรับทั้งหมด',bd=6,width=10,bg='white')
label4.place(x=10,y=350)
label5 = tk.Label(C2,text='รายจ่ายทั้งหมด',bd=6,width=10,bg='white')
label5.place(x=10,y=400)
label6 = tk.Label(C2,bd=6,text='',bg='white')
label6.place(x=100,y=350)
label7 = tk.Label(C2,bd=6,text='',bg='white')
label7.place(x=100,y=400)
label8 = tk.Label(C2,text='เงินคงเหลือ',bd=6,width=10,bg='white')
label8.place(x=10,y=450)
label9 = tk.Label(C2,bd=6,text='',bg='white')
label9.place(x=100,y=450)

E1 = Entry(C1, bd =5.5,)
E1.insert(0, "Type item name here...")
E1.place(x=100,y=130)
E2 = Entry(C1, bd =5.5,)
E2.insert(0, "Type price here...")
E2.place(x=280,y=130)

var = StringVar(app,'รับ(+)') #IntVar(value='1')
r1 = Radiobutton(C1, text = 'รายรับ',bd=4, value = 'รับ(+)',variable=var,background = "light blue")
r1.place(x=100,y=100)   
r2 = Radiobutton(C1, text = 'รายจ่าย',bd=4, value = 'จ่าย(-)',variable=var,background = "light blue")
r2.place(x=160,y=100)

 

############ part load data to table ############
container01_table = tk.Frame(C2)
canvas = tk.Canvas(container01_table)
scrollbar = tk.Scrollbar(container01_table, orient="vertical", command=canvas.yview)
scrollable_frame = tk.Frame(canvas)

scrollable_frame.bind(
    "<Configure>",
    lambda e: canvas.configure(
        scrollregion=canvas.bbox("all"),
        height=200,
        width=450,
    )
)

canvas.create_window((0, 0), window=scrollable_frame, anchor="nw")
canvas.configure(yscrollcommand=scrollbar.set)

df = pd.read_csv('member.csv')
lst = []
for i in range(len(df['no'])):
  row = (df.iloc[i]['no'],df.iloc[i]['item'],df.iloc[i]['type'],df.iloc[i]['amount'])
  lst.append(row)
  
# find total number of rows and
# columns in list
total_rows = len(lst)
total_columns = len(lst[0])

t = Table(scrollable_frame,lst,total_rows,total_columns)

container01_table.place(x=10,y=100)
canvas.pack(side="left", fill="both", expand=True)
scrollbar.pack(side="right", fill="y")
############ end part load data to table ############

C1.tkraise()
app.resizable(False, False)

app.geometry("500x500")
app.mainloop()