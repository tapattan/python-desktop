#https://blog.teclado.com/tkinter-scrollable-frames/

import tkinter as tk
from tkinter import ttk
import pandas as pd 

class Table:
     
    def __init__(self,root,lst,total_rows,total_columns):
        # code for creating table
        for i in range(total_rows):
            for j in range(total_columns):
                if(j==0): #กำหนดให้ 0 คือ columns แรก
                  w_ = 1
                elif(j==1):
                  w_ = 50
                else:
                  w_ = 7
                 
                self.e = tk.Entry(root, width=w_, fg='black',
                               font=('Arial',10,'bold'))
                 
                self.e.grid(row=i, column=j)
                self.e.insert(tk.END, lst[i][j])
                print('call table',lst[i][j]) 



root = tk.Tk()
container = ttk.Frame(root)
canvas = tk.Canvas(container)
scrollbar = ttk.Scrollbar(container, orient="vertical", command=canvas.yview)
scrollable_frame = ttk.Frame(canvas)

scrollable_frame.bind(
    "<Configure>",
    lambda e: canvas.configure(
        scrollregion=canvas.bbox("all"),
        height=100
    )
)

canvas.create_window((0, 0), window=scrollable_frame, anchor="nw")

canvas.configure(yscrollcommand=scrollbar.set)

#for i in range(50):
#    ttk.Label(scrollable_frame, text="Sample scrolling label").pack()

df = pd.read_csv('member.csv')
lst = []
for i in range(len(df['no'])):
  row = (df.iloc[i]['no'],df.iloc[i]['item'],df.iloc[i]['type'],df.iloc[i]['amount'])
  lst.append(row)
  
# find total number of rows and
# columns in list
total_rows = len(lst)
total_columns = len(lst[0])

t = Table(scrollable_frame,lst,total_rows,total_columns)

container.place(x=50,y=10)
canvas.pack(side="left", fill="both", expand=True)
scrollbar.pack(side="right", fill="y")

root.mainloop()