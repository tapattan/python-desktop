def print_something():
    print('Hi..'+str(w.get()))
    print('Hi..'+current_value.get())
   
def spinbox1_callback(): #event call on change
     print('onchange..'+current_value.get())
     
import tkinter as tk
from tkinter import Spinbox
from tkinter import StringVar

app = tk.Tk()
app.title('Hello world')

#เพิ่มปุ่มเข้ามา
label1 = tk.Label(app,text='อายุของคุณ : ',width=12)
#label1.place(x=0,y=0)
label1.grid(row=0,column=0,sticky='e')

current_value = StringVar(value=0)

w = Spinbox(app, from_=0, to=60,width=8,textvariable=current_value,values=(0,10,20,30,40,50,60) 
            ,command=spinbox1_callback)
w.grid(row=0,column=1,sticky='e')


button = tk.Button(app,text='click me!',bg = "yellow", command=print_something)
#button.place(x=50,y=0) #replace values with required
#button.pack()  #comment this
button.grid(row=4,column=1,sticky='w')

app.geometry("300x300")
app.mainloop()