import tkinter as tk
from tkinter import ttk as ttk
app = tk.Tk()
app.title("Tab Widget")


########## กำหนด style ใน mac ###########
style = ttk.Style()
style.theme_create("custom_tabs", parent="alt", settings={
    "TNotebook.Tab": {
        "configure": {"padding": [10, 10, 10, 10]}
        }})
style.theme_use("custom_tabs")
#########################################


tabControl = ttk.Notebook(app,width=300, height=300)
  
objSummaryTab = tk.Frame(tabControl)
objSettingsTab = tk.Frame(tabControl)
  
tabControl.add(objSummaryTab, text ='Summary')
tabControl.add(objSettingsTab, text ='Settings')
  
lb1 = tk.Label(objSummaryTab, text ="Summary")
lb1.place(x=5, y=5)  
          
lb2 = tk.Label(objSettingsTab, text ="Settings")
lb2.place(x=5, y=5) 

tabControl.place(x=10, y=10)

app.bind("<Escape>", lambda _: app.destroy())

app.geometry("500x500")

app.mainloop()