def print_something():
    #print('Button bind working!')
    df = pd.read_csv('member.csv')
    if(len(df)==0):
       no_next = 1
    else:  
       no_next = df['no'].max()+1
       
    code =  textbox.get()
    name = var.get()

    new_item = pd.DataFrame({'no':[no_next],'code':[code],'name':[name]})
    print(new_item)

    df = pd.concat([df,new_item])
    df.to_csv('member.csv',index=False)

def refreshData():

    for i in my_game.get_children():
      my_game.delete(i)

    df = pd.read_csv('member.csv')
    k = []
    for i in df.to_numpy():
      k.append(list(i))

    datasource =  k
    print(datasource)
    for i in range(len(datasource)):
      #add data  
      my_game.insert(parent='',index='end',iid=i,text='',
      values=(datasource[i]))


def test(event=None):
        print(event,event.x)
        if(event.x>100 and event.x<150):
           print('tab2')
           refreshData() 

        #Imagine the code for selecting the text widget is here.
        return 0

import tkinter as tk
from tkinter import ttk as ttk

from tkinter import Radiobutton,IntVar,StringVar
import pandas as pd 

app = tk.Tk()
app.title("บันทึกรายการ รายรับรายจ่าย")


########## กำหนด style ใน mac ###########
style = ttk.Style()
style.theme_create("custom_tabs", parent="alt", settings={
    "TNotebook.Tab": {
        "configure": {"padding": [10, 10, 10, 10]}
        }})
style.theme_use("custom_tabs")
#########################################


tabControl = ttk.Notebook(app,width=550, height=300)
  
objAddItemTab = tk.Frame(tabControl)
objReportTab = tk.Frame(tabControl)
  
tabControl.add(objAddItemTab, text ='บันทึกรายการ')
tabControl.add(objReportTab, text ='สรุปผล')
  
tabControl.bind('<ButtonRelease-1>', test)

lb1 = tk.Label(objAddItemTab, text ="ประเภท")
lb1.place(x=5, y=5)  
var = StringVar(value='รายรับ') #StringVar() 
r1 = Radiobutton(objAddItemTab, text = 'รายรับ', value = 'รายรับ',variable=var,background = "light blue")
r1.place(x=60,y=5)   
r2 = Radiobutton(objAddItemTab, text = 'รายจ่าย', value = 'รายจ่าย',variable=var,background = "light blue")
r2.place(x=120,y=5)


lb1 = tk.Label(objAddItemTab, text ="รายการ")
lb1.place(x=5, y=40)  
textbox = tk.Entry(objAddItemTab)
textbox.place(x=60,y=40)
textbox.config(font =("Courier", 14))


button = tk.Button(objAddItemTab,text='click me!', command=print_something)
button.place(x=60, y=65) 


####################################################
lb2 = tk.Label(objReportTab, text ="สรุปรายงานการใช้จ่าย")
lb2.place(x=5, y=5) 

game_frame = tk.Frame(objReportTab)
game_frame.place(x=10,y=30)

#scrollbar
game_scroll = tk.Scrollbar(game_frame)
game_scroll.pack(side=tk.RIGHT, fill=tk.Y)

game_scroll = tk.Scrollbar(game_frame,orient='horizontal')
#game_scroll.pack(side= tk.BOTTOM,fill=tk.X)

my_game = ttk.Treeview(game_frame,yscrollcommand=game_scroll.set, xscrollcommand =game_scroll.set )
my_game.pack()

game_scroll.config(command=my_game.yview)
#game_scroll.config(command=my_game.xview)

#define our column
 
my_game['columns'] = ('id', 'type', 'detail')

# format our column
my_game.column("#0", width=0,  stretch=tk.NO)
my_game.column("id",anchor=tk.CENTER, width=50)
my_game.column("type",anchor=tk.CENTER,width=50)
my_game.column("detail",anchor=tk.CENTER,width=300)


#Create Headings 
my_game.heading("#0",text="",anchor=tk.CENTER)
my_game.heading("id",text="id",anchor=tk.CENTER)
my_game.heading("type",text="type",anchor=tk.CENTER)
my_game.heading("detail",text="detail",anchor=tk.CENTER)


refreshData()


####################################################
tabControl.place(x=10, y=10)

app.bind("<Escape>", lambda _: app.destroy())

app.geometry("650x500")

app.mainloop()