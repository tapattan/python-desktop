from tkinter import *
from  tkinter import ttk

ws  = Tk()
ws.title('PythonGuides')
ws.geometry('500x500')
ws['bg'] = '#AC99F2'

game_frame = Frame(ws)
game_frame.place(x=20,y=100)

#scrollbar
game_scroll = Scrollbar(game_frame)
game_scroll.pack(side=RIGHT, fill=Y)

game_scroll = Scrollbar(game_frame,orient='horizontal')
game_scroll.pack(side= BOTTOM,fill=X)

my_game = ttk.Treeview(game_frame,yscrollcommand=game_scroll.set, xscrollcommand =game_scroll.set)

my_game.pack()

game_scroll.config(command=my_game.yview)
game_scroll.config(command=my_game.xview)

#define our column
 
my_game['columns'] = ('player_id', 'player_name', 'player_Rank', 'player_states', 'player_city')

# format our column
my_game.column("#0", width=0,  stretch=NO)
my_game.column("player_id",anchor=CENTER, width=80)
my_game.column("player_name",anchor=CENTER,width=80)
my_game.column("player_Rank",anchor=CENTER,width=80)
my_game.column("player_states",anchor=CENTER,width=80)
my_game.column("player_city",anchor=CENTER,width=80)

#Create Headings 
my_game.heading("#0",text="",anchor=CENTER)
my_game.heading("player_id",text="Id",anchor=CENTER)
my_game.heading("player_name",text="Name",anchor=CENTER)
my_game.heading("player_Rank",text="Rank",anchor=CENTER)
my_game.heading("player_states",text="States",anchor=CENTER)
my_game.heading("player_city",text="States",anchor=CENTER)


datasource = [['1','Ninja','101','Oklahoma', 'Moore'],
              ['2','Ranger','102','Wisconsin', 'Green Bay'],
              ['3','Deamon','103', 'California', 'Placentia'],
              ['4','Dragon','104','New York' , 'White Plains'],
              ['5','CrissCross','105','California', 'San Diego'],
              ['6','ZaqueriBlack','106','Wisconsin' , 'TONY'],
              ['7','RayRizzo','107','Colorado' , 'Denver'],
              ['8','Byun','108','Pennsylvania' , 'ORVISTON'],
              ['9','Trink','109','Ohio' , 'Cleveland'],
              ['10','Twitch','110','Georgia' , 'Duluth'],
              ['11','Animus','111', 'Connecticut' , 'Hartford']] 


for i in range(len(datasource)):
   #add data  
   my_game.insert(parent='',index='end',iid=i,text='',
   values=(datasource[i]))


ws.mainloop()



