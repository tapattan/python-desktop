#lab เกม tic tac toe

def checkAval(c):
    k = c.cget('text')
    if(k!=''):
        return False 
    return True 

def checkWin(board):
    win = [[1,2,3],[4,5,6],[7,8,9],[1,4,7],[2,5,8],[3,6,9],[1,5,9],[3,5,7]]
    for i in win:
      score = 0  
      for j in i:
        if(j in board):
           score+=1 
      if(score==3):
         return True 

    return False 

def selectMap(k,player1,player2):
    if(player1.get()):
     player1.set(False)
     player2.set(True)   
     if(k==1):
       if(checkAval(button1)):
         button1.config(text="X")
     if(k==2):
       if(checkAval(button2)):
         button2.config(text="X") 
     if(k==3):
       if(checkAval(button3)):
         button3.config(text="X")
     if(k==4):
       if(checkAval(button4)):
         button4.config(text="X")  
     if(k==5):
       if(checkAval(button5)):
         button5.config(text="X")
     if(k==6):
       if(checkAval(button6)):
         button6.config(text="X") 
     if(k==7):
       if(checkAval(button7)):
         button7.config(text="X")
     if(k==8):
       if(checkAval(button8)):
         button8.config(text="X") 
     if(k==9):
       if(checkAval(button9)):
         button9.config(text="X")      
     
     histX.append(k)
     k = checkWin(histX)
     if(k):
       print('X Win')


    elif(player2.get()):
     player1.set(True)
     player2.set(False)    
     if(k==1):
       if(checkAval(button1)):
         button1.config(text="O")
     if(k==2):
       if(checkAval(button2)):
         button2.config(text="O") 
     if(k==3):
       if(checkAval(button3)):
         button3.config(text="O")
     if(k==4):
       if(checkAval(button4)):
         button4.config(text="O")  
     if(k==5):
       if(checkAval(button5)):
         button5.config(text="O")
     if(k==6):
       if(checkAval(button6)):
         button6.config(text="O") 
     if(k==7):
       if(checkAval(button7)):
         button7.config(text="O")
     if(k==8):
       if(checkAval(button8)):
         button8.config(text="O") 
     if(k==9):
       if(checkAval(button9)):
         button9.config(text="O") 
     
     histO.append(k)
     k = checkWin(histO)
     if(k):
       print('O Win')

    
import tkinter as tk
from functools import partial
app = tk.Tk()
app.title('tic tac toe')

player1 = tk.BooleanVar(app)
player2 = tk.BooleanVar(app)

player1.set(True)
player2.set(False)

histX = [] #can use global 
histO = [] #can use global

button1 = tk.Button(text='',width=6,height=3,command=partial(selectMap,1,player1,player2))
button1.place(x=10,y=10)
button2 = tk.Button(text='',width=6,height=3, command=partial(selectMap,2,player1,player2))
button2.place(x=80,y=10)
button3 = tk.Button(text='',width=6,height=3 ,command=partial(selectMap,3,player1,player2))
button3.place(x=150,y=10)

button4 = tk.Button(text='',width=6,height=3, command=partial(selectMap,4,player1,player2))
button4.place(x=10,y=80)
button5 = tk.Button(text='',width=6,height=3, command=partial(selectMap,5,player1,player2))
button5.place(x=80,y=80)
button6 = tk.Button(text='',width=6,height=3 ,command=partial(selectMap,6,player1,player2))
button6.place(x=150,y=80)

button7 = tk.Button(text='',width=6,height=3, command=partial(selectMap,7,player1,player2))
button7.place(x=10,y=150)
button8 = tk.Button(text='',width=6,height=3, command=partial(selectMap,8,player1,player2))
button8.place(x=80,y=150)
button9 = tk.Button(text='',width=6,height=3 ,command=partial(selectMap,9,player1,player2))
button9.place(x=150,y=150)


app.geometry("220x220")
app.mainloop()