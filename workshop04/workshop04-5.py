#lab เกม tic tac toe

def checkAval(c):
    k = c.cget('text')
    if(k!=''):
        return False 
    return True 

def checkWin(board):
    win = [[1,2,3,4,5],[6,7,8,9,10],[11,12,13,14,15],[16,17,18,19,20],[21,22,23,24,25],
           [1,6,11,16,21],[2,7,12,17,22],[3,8,13,18,23],[4,9,14,19,24],[5,10,15,20,25],
           [1,7,13,19,25],[5,9,13,17,21]]
    for i in win:
      score = 0  
      for j in i:
        if(j in board):
           score+=1 
      if(score==5):
         return True 

    return False 

def newgame():
    button1.config(text="")
    button2.config(text="")
    button3.config(text="")
    button4.config(text="")
    button5.config(text="")
    button6.config(text="")
    button7.config(text="")
    button8.config(text="")
    button9.config(text="")
    button10.config(text="")
    button11.config(text="")
    button12.config(text="")
    button13.config(text="")
    button14.config(text="")
    button15.config(text="")
    button16.config(text="")
    button17.config(text="")
    button18.config(text="")
    button19.config(text="")
    button20.config(text="")
    button21.config(text="")
    button22.config(text="")
    button23.config(text="")
    button24.config(text="")
    button25.config(text="")
    player1.set(True)
    player2.set(False)

    statusGameEnd.set(False)
    labelD.config(text='ตาเดินของ X',fg='black')
    
    for i in range(len(histO)):
        histO.pop(0)
    
    for i in range(len(histX)):
        histX.pop(0)
     

def selectMap(k,player1,player2):
    if(statusGameEnd.get()):
      return 0 

    if(player1.get()):
     player1.set(False)
     player2.set(True)   
     if(k==1):
       if(checkAval(button1)):
         button1.config(text="X")
     if(k==2):
       if(checkAval(button2)):
         button2.config(text="X") 
     if(k==3):
       if(checkAval(button3)):
         button3.config(text="X")
     if(k==4):
       if(checkAval(button4)):
         button4.config(text="X")  
     if(k==5):
       if(checkAval(button5)):
         button5.config(text="X")
     if(k==6):
       if(checkAval(button6)):
         button6.config(text="X") 
     if(k==7):
       if(checkAval(button7)):
         button7.config(text="X")
     if(k==8):
       if(checkAval(button8)):
         button8.config(text="X") 
     if(k==9):
       if(checkAval(button9)):
         button9.config(text="X") 
     if(k==9):
       if(checkAval(button9)):
         button9.config(text="X")
     if(k==9):
       if(checkAval(button9)):
         button9.config(text="X")
     if(k==9):
       if(checkAval(button9)):
         button9.config(text="X")
     if(k==9):
       if(checkAval(button9)):
         button9.config(text="X")
     if(k==9):
       if(checkAval(button9)):
         button9.config(text="X")
     if(k==9):
       if(checkAval(button9)):
         button9.config(text="X")
     if(k==9):
       if(checkAval(button9)):
         button9.config(text="X")
     if(k==10):
       if(checkAval(button10)):
         button10.config(text="X")
     if(k==11):
       if(checkAval(button11)):
         button11.config(text="X")
     if(k==12):
       if(checkAval(button12)):
         button12.config(text="X")
     if(k==13):
       if(checkAval(button13)):
         button13.config(text="X")
     if(k==14):
       if(checkAval(button14)):
         button14.config(text="X")
     if(k==15):
       if(checkAval(button15)):
         button15.config(text="X")
     if(k==16):
       if(checkAval(button16)):
         button16.config(text="X")
     if(k==17):
       if(checkAval(button17)):
         button17.config(text="X")
     if(k==18):
       if(checkAval(button18)):
         button18.config(text="X")  
     if(k==19):
       if(checkAval(button19)):
         button19.config(text="X")
     if(k==20):
       if(checkAval(button20)):
         button20.config(text="X")
     if(k==21):
       if(checkAval(button21)):
         button21.config(text="X")
     if(k==22):
       if(checkAval(button22)):
         button22.config(text="X")
     if(k==23):
       if(checkAval(button23)):
         button23.config(text="X")
     if(k==24):
       if(checkAval(button24)):
         button24.config(text="X")
     if(k==25):
       if(checkAval(button25)):
         button25.config(text="X")                                
     
     labelD.config(text='ตาเดินของ O',fg='black')
     histX.append(k)
     k = checkWin(histX)
     if(k):
       print('X Win')
       labelD.config(text='X Win' ,fg='red')
       statusGameEnd.set(True)

    elif(player2.get()):
     player1.set(True)
     player2.set(False)    
     if(k==1):
       if(checkAval(button1)):
         button1.config(text="O")
     if(k==2):
       if(checkAval(button2)):
         button2.config(text="O") 
     if(k==3):
       if(checkAval(button3)):
         button3.config(text="O")
     if(k==4):
       if(checkAval(button4)):
         button4.config(text="O")  
     if(k==5):
       if(checkAval(button5)):
         button5.config(text="O")
     if(k==6):
       if(checkAval(button6)):
         button6.config(text="O") 
     if(k==7):
       if(checkAval(button7)):
         button7.config(text="O")
     if(k==8):
       if(checkAval(button8)):
         button8.config(text="O") 
     if(k==9):
       if(checkAval(button9)):
         button9.config(text="O") 
     if(k==10):
       if(checkAval(button10)):
         button10.config(text="O") 
     if(k==11):
       if(checkAval(button11)):
         button11.config(text="O") 
     if(k==12):
       if(checkAval(button12)):
         button12.config(text="O") 
     if(k==13):
       if(checkAval(button13)):
         button13.config(text="O") 
     if(k==14):
       if(checkAval(button14)):
         button14.config(text="O") 
     if(k==15):
       if(checkAval(button15)):
         button15.config(text="O") 
     if(k==16):
       if(checkAval(button16)):
         button16.config(text="O") 
     if(k==17):
       if(checkAval(button17)):
         button17.config(text="O") 
     if(k==18):
       if(checkAval(button18)):
         button18.config(text="O") 
     if(k==19):
       if(checkAval(button19)):
         button19.config(text="O") 
     if(k==20):
       if(checkAval(button20)):
         button20.config(text="O") 
     if(k==21):
       if(checkAval(button21)):
         button21.config(text="O") 
     if(k==22):
       if(checkAval(button22)):
         button22.config(text="O") 
     if(k==23):
       if(checkAval(button23)):
         button23.config(text="O") 
     if(k==24):
       if(checkAval(button24)):
         button24.config(text="O") 
     if(k==25):
       if(checkAval(button25)):
         button25.config(text="O")         


     labelD.config(text='ตาเดินของ X',fg='black')
     
     histO.append(k)
     k = checkWin(histO)
     if(k):
       print('O Win')
       labelD.config(text='O Win' ,fg='blue')
       statusGameEnd.set(True)
     
    ################### 
    print(histO)
    print(histX)
    if(len(histO)+len(histX)==25 and k==False):
      labelD.config(text='คุณเสมอกัน' ,fg='black')  
      statusGameEnd.set(True)


 
import tkinter as tk
from functools import partial
app = tk.Tk()
app.title('tic tac toe')

player1 = tk.BooleanVar(app)
player2 = tk.BooleanVar(app)
statusGameEnd = tk.BooleanVar(app)

player1.set(True)
player2.set(False)

statusGameEnd.set(False)

histX = [] #can use global 
histO = [] #can use global

labelD = tk.Label(text='ตาเดินของ X',font=("Helvetica", 20))
labelD.place(x=80,y=10)

newgamebtn = tk.Button(text='new game',width=8,height=1,command=partial(newgame))
newgamebtn.place(x=220,y=10)

button1 = tk.Button(text='',width=6,height=3,command=partial(selectMap,1,player1,player2))
button1.place(x=10,y=10+50)
button2 = tk.Button(text='',width=6,height=3, command=partial(selectMap,2,player1,player2))
button2.place(x=80,y=10+50)
button3 = tk.Button(text='',width=6,height=3 ,command=partial(selectMap,3,player1,player2))
button3.place(x=150,y=10+50)
button4 = tk.Button(text='',width=6,height=3 ,command=partial(selectMap,4,player1,player2))
button4.place(x=220,y=10+50)
button5 = tk.Button(text='',width=6,height=3 ,command=partial(selectMap,5,player1,player2))
button5.place(x=290,y=10+50)

button6 = tk.Button(text='',width=6,height=3, command=partial(selectMap,6,player1,player2))
button6.place(x=10,y=80+50)
button7 = tk.Button(text='',width=6,height=3, command=partial(selectMap,7,player1,player2))
button7.place(x=80,y=80+50)
button8 = tk.Button(text='',width=6,height=3 ,command=partial(selectMap,8,player1,player2))
button8.place(x=150,y=80+50)
button9 = tk.Button(text='',width=6,height=3 ,command=partial(selectMap,9,player1,player2))
button9.place(x=220,y=80+50)
button10 = tk.Button(text='',width=6,height=3 ,command=partial(selectMap,10,player1,player2))
button10.place(x=290,y=80+50)

button11 = tk.Button(text='',width=6,height=3, command=partial(selectMap,11,player1,player2))
button11.place(x=10,y=150+50)
button12 = tk.Button(text='',width=6,height=3, command=partial(selectMap,12,player1,player2))
button12.place(x=80,y=150+50)
button13 = tk.Button(text='',width=6,height=3 ,command=partial(selectMap,13,player1,player2))
button13.place(x=150,y=150+50)
button14 = tk.Button(text='',width=6,height=3 ,command=partial(selectMap,14,player1,player2))
button14.place(x=220,y=150+50)
button15 = tk.Button(text='',width=6,height=3 ,command=partial(selectMap,15,player1,player2))
button15.place(x=290,y=150+50)

button16 = tk.Button(text='',width=6,height=3, command=partial(selectMap,16,player1,player2))
button16.place(x=10,y=220+50)
button17 = tk.Button(text='',width=6,height=3, command=partial(selectMap,17,player1,player2))
button17.place(x=80,y=220+50)
button18 = tk.Button(text='',width=6,height=3 ,command=partial(selectMap,18,player1,player2))
button18.place(x=150,y=220+50)
button19 = tk.Button(text='',width=6,height=3 ,command=partial(selectMap,19,player1,player2))
button19.place(x=220,y=220+50)
button20 = tk.Button(text='',width=6,height=3 ,command=partial(selectMap,20,player1,player2))
button20.place(x=290,y=220+50)

button21 = tk.Button(text='',width=6,height=3, command=partial(selectMap,21,player1,player2))
button21.place(x=10,y=290+50)
button22 = tk.Button(text='',width=6,height=3, command=partial(selectMap,22,player1,player2))
button22.place(x=80,y=290+50)
button23 = tk.Button(text='',width=6,height=3 ,command=partial(selectMap,23,player1,player2))
button23.place(x=150,y=290+50)
button24 = tk.Button(text='',width=6,height=3 ,command=partial(selectMap,24,player1,player2))
button24.place(x=220,y=290+50)
button25 = tk.Button(text='',width=6,height=3 ,command=partial(selectMap,25,player1,player2))
button25.place(x=290,y=290+50)

app.resizable(False, False)
app.geometry("400x480")
app.mainloop()