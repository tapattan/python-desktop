#pip install tkintermapview
#https://github.com/TomSchimansky/TkinterMapView 

import tkinter
from tkintermapview import TkinterMapView

app = tkinter.Tk()
app.title("map_view_simple_example.py")

# create map widget
map_widget = TkinterMapView(app, width=600, height=400, corner_radius=0)
map_widget.place(x=20,y=20)

# google normal tile server
marker_1 = map_widget.set_position(13.825558421906006, 100.56840075979935, marker=True)
marker_1.set_text('Asiamediasoft ตึกช้าง รัชโยธิน')
map_widget.set_zoom(15)

app.geometry(f"{800}x{500}")
app.mainloop()