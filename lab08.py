def print_something():
    print('Hi..' +__valuesRadio__.get())
   
import tkinter as tk
from functools import partial

from tkinter import StringVar 
from tkinter import Radiobutton

app = tk.Tk()
app.title('Hello world')

#เพิ่มปุ่มเข้ามา
label1 = tk.Label(app,text='เพศ : ',width=5)
#label1.place(x=0,y=0)
label1.grid(row=0,column=0,sticky='e')

__valuesRadio__ = StringVar(app, "1") #เปลี่ยนค่าของ StringVar ให้สามารถ selected ค่าได้
values = {"ชาย " : "1",
          "หญิง " : "2",
          "อื่นๆ " : "3",}
 
# Loop is used to create multiple Radiobuttons
# rather than creating each button separately
col = 1
for (text, value) in values.items():
    Radiobutton(app, text = text, variable = __valuesRadio__,
                value = value,
                background = "light blue").grid(row=0,column=col,sticky='e')   
    col+=1

button = tk.Button(app,text='click me!',bg = "yellow", command=print_something)
#button.place(x=50,y=0) #replace values with required
#button.pack()  #comment this
button.grid(row=1,column=1,sticky='w')

app.geometry("300x300")
app.mainloop()