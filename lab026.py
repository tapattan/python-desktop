import tkinter as tk
 
def create_window():
    window = tk.Toplevel(root)
    window.geometry("300x300")
    window.title("New Window")
    tk.Label(window, text="Hello David!", font=('Helvetica 17 bold')).pack(pady=30)

root = tk.Tk()
b = tk.Button(root, text="Create new window", command=create_window)
b.pack()

root.geometry("300x300")
root.mainloop()