def show_frame(cont):
        if(cont==1):
          container01.tkraise()
        if(cont==2):
          container02.tkraise()  
        if(cont==3):
          container03.tkraise()   

import tkinter as tk
from functools import partial
app = tk.Tk()

app.title('แบบฝึกหัดของชั้น')

container01 = tk.Frame(app,height=200,width=500,bg='black') 
label1 = tk.Label(container01,text='นี่คือ View 01',width=10,bg='white')
label1.place(x=50,y=20)
button1 = tk.Button(container01,text='View1',command=partial(show_frame,1))
button1.place(x=50,y=50)
button2 = tk.Button(container01,text='View2',command=partial(show_frame,2))
button2.place(x=150,y=50)
button3 = tk.Button(container01,text='View3',command=partial(show_frame,3))
button3.place(x=250,y=50)
container01.place(x=10,y=10)


container02 = tk.Frame(app,height=200,width=500,bg='red') 
label1 = tk.Label(container02,text='นี่คือ View 02',width=10,bg='white')
label1.place(x=50,y=20)
button1 = tk.Button(container02,text='View1',command=partial(show_frame,1))
button1.place(x=50,y=50)
button2 = tk.Button(container02,text='View2',command=partial(show_frame,2))
button2.place(x=150,y=50)
button3 = tk.Button(container02,text='View3',command=partial(show_frame,3))
button3.place(x=250,y=50)
container02.place(x=10,y=10)


container03 = tk.Frame(app,height=200,width=500,bg='blue') 
label1 = tk.Label(container03,text='นี่คือ View 03',width=10,bg='white')
label1.place(x=50,y=20)
button1 = tk.Button(container03,text='View1',command=partial(show_frame,1))
button1.place(x=50,y=50)
button2 = tk.Button(container03,text='View2',command=partial(show_frame,2))
button2.place(x=150,y=50)
button3 = tk.Button(container03,text='View3',command=partial(show_frame,3))
button3.place(x=250,y=50)
container03.place(x=10,y=10)

container01.tkraise()

app.geometry('600x300')
app.mainloop()