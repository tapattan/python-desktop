def show_frame(cont):
        if(cont==1):
          container01.tkraise()
        if(cont==2):
          container02.tkraise()  

import tkinter as tk
from functools import partial
app = tk.Tk()

app.title('แบบฝึกหัดของชั้น')

container01 = tk.Frame(app,height=200,width=200,bg='black') 
label1 = tk.Label(container01,text='นี่คือ View 01',width=10,bg='white')
label1.place(x=50,y=20)
button = tk.Button(container01,text='click me01!',command=partial(show_frame,2))
button.place(x=50,y=50)
container01.place(x=10,y=10)


container02 = tk.Frame(app,height=200,width=200,bg='red') 
label1 = tk.Label(container02,text='นี่คือ View 02',width=10,bg='white')
label1.place(x=50,y=20)
button = tk.Button(container02,text='click me02!',command=partial(show_frame,1))
button.place(x=50,y=50)
container02.place(x=10,y=10)

container01.tkraise()

app.geometry('400x300')
app.mainloop()