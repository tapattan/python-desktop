import tkinter as tk

# create the root window
app = tk.Tk()

app.resizable(True, True)
app.title('Listbox')
 
# create a list box
langs = ('Java', 'C#', 'C', 'C++', 'Python',
        'Go', 'JavaScript', 'PHP', 'Swift')

langs_var = tk.StringVar(value=langs)

listbox = tk.Listbox(
    app,
    listvariable=langs_var,
    height=1,
    selectmode='browse')

listbox.grid(
    column=0,
    row=0,
    sticky='e'
)

app.geometry("300x300")
app.mainloop()