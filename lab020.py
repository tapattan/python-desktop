from tkinter import *
from tkinter.ttk import Progressbar
import time
import threading
from functools import partial

ws = Tk()
ws.title('PythonGuides')

def processJob(k):
    print(1)
    pb['value'] = 0
    k.start()
  
def play():
    for i in range(5):
        ws.update_idletasks()
        pb['value'] += 20
        print(pb['value'])
        time.sleep(1)
    
pb = Progressbar(
    ws,
    orient = HORIZONTAL,
    length = 100,
    mode = 'determinate'
    )
pb.pack(pady=30)

play = Button(
    ws,
    text="start",
    command=partial(processJob,threading.Thread(target=play)))
    #command=play)
play.pack(pady=30)

ws.geometry('300x300')
ws.mainloop()