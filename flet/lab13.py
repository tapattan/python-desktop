import flet
from flet import Image, Page, Container,Row,Column, Stack, Text,padding,border_radius

def main(page: Page):
    st = Stack(
        [
            Image(
                src=f"images/JrI9qv.jpg",
                width=500,
                height=500,
                fit="fill",
                repeat="repeat",
            ),
            Container(
                content=Row([Image(
                    src=f"images/cat.jpg",
                    width=50,
                    height=50,
                    fit="contain",
                    border_radius=border_radius.all(45))] ,
                alignment='center',
                ),
                padding=padding.only(top=80)),
            Container(
                content=Row(
                [
                    Text(
                        "Welcome to Member Club",
                        color="white",
                        size=14,
                        weight="bold",
                        opacity=0.5,
                    )
                ],
                alignment="center",
            ),
            padding=padding.only(top=50)),
        ],
        width=300,
        height=300,
    )

    page.add(st)

flet.app(target=main, assets_dir="assets",view=flet.WEB_BROWSER)