from email.mime import image
from turtle import width
import flet
from flet import AlertDialog,Icon,View, AppBar,Banner,ElevatedButton,FilledButton, Page,Row,Column, TextField,Text, TextButton,Image,Container,alignment,colors,LinearGradient,padding,icons

def main(page: Page):
    page.title = "Member Club"
    page.theme_mode = "light"
    page.padding = 0 #ให้รูปด้านหลังเต็ม
    bg = Image(src='images/JrI9qv.jpg',
                fit="fill",
                repeat="repeat",
                width=400,
                height=400,)
    
    def close_dlg(e):
        dlg_modal.open = False
        page.update()


    dlg_modal = AlertDialog(
        modal=True,
        title=Text("Please confirm"),
        content=Text("Do you really want to delete all those files?"),
        actions=[
            TextButton("Yes", on_click=close_dlg),
            TextButton("No", on_click=close_dlg),
        ],
        actions_alignment="end",
        on_dismiss=lambda e: print("Modal dialog dismissed!"),
    )
    
    def close_banner(e):
        page.banner.open = False
        page.update()

    page.banner = Banner(
        bgcolor=colors.AMBER_100,
        leading=Icon(icons.WARNING_AMBER_ROUNDED, color=colors.AMBER, size=40),
        content=Text(
            "Oops, Login Failed!\nYour username or password is incorrect.",size=10,
        ),
        actions=[
            #TextButton("Retry", on_click=close_banner),
            #TextButton("Ignore", on_click=close_banner),
            TextButton("Cancel", on_click=close_banner),
        ],
    )


    def view_pop(view):
        page.views.pop()
        top_view = page.views[-1]
        page.go(top_view.route)

    welcomeText = Text(
    value="Welcome To Member Club.",
    size=20,
    color="pink",
    #bgcolor="pink",
    weight="bold",
    italic=True,)

    def button_clicked(e):
        u = txtUsername.value
        p = txtPassword.value
        txtUsername.value='' 
        txtPassword.value=''
        if(u=='' and p==''):
          return 0
        elif(u=='admin' and p=='12345678'):
           pass 
           page.views.append(
                View(
                    "/store",
                    [   
                        welcomeText,
                        #AppBar(title=Text("MemberClub"), bgcolor=colors.SURFACE_VARIANT),
                        ElevatedButton("LOGOUT", on_click=view_pop),
                    ],
                )
            )
           page.update()

        else:
           page.banner.open = True
           page.update()
 
    # font ของ icons https://fonts.google.com/icons?icon.query=user&icon.set=Material+Icons&icon.platform=flutter
    txtUsername = TextField(value="",label="UserName",prefix_icon=icons.PERSON,border_color='white',text_align="left",color='white',width=200,height=30,text_size=10)
    txtPassword = TextField(value="",label="Password",prefix_icon=icons.PASSWORD, 
                            password=True, can_reveal_password=True,border_color='white',text_align="left",color='white',width=200,height=30,text_size=10)
    btnLogin = FilledButton("LOGIN", icon="login",icon_color='WHITE',width=200,height=30,on_click=button_clicked)
   
    page.add(Container(content=Column([txtUsername,txtPassword,Row(),Row(),btnLogin]),
        image_src='images/JrI9qv2.jpg',
        width=400,
        height=400,
        image_fit='fill',
        image_repeat='repeat',
        border_radius=0,
        padding=padding.only(top=100,left=100),
    ))

    ###############

    page.window_width = 400
    page.window_height = 400
    page.window_max_width = 400
    page.window_min_width = 400
    page.window_max_height = 400
    page.window_min_height = 400
    page.update()
flet.app(target=main,assets_dir="assets",)