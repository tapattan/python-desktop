from turtle import width
import flet
from flet import TextField,ElevatedButton, Row ,Column,Container,colors

from functools import partial

def main(page):
    page.bgcolor = 'black'

    page.window_width = 380
    page.window_height = 380
    page.window_max_width = 380
    page.window_min_width = 380
    page.window_max_height = 380
    page.window_min_height = 380
    page.update()


    def btn_click(data,e):
        if(data=='='):
          k = display.value
          if('π' in k):
              k = k.replace('π','(22/7)')
          if('×' in k):
             k = k.replace('×','*')
          if('÷' in k):
             k = k.replace('÷','/')
          if('²' in k):
             k = k.replace('²','**2')  
          if('√' in k):
            tmp = ''
            for i in range(len(k)):
             if(k[i] =='√'):
                pos = 'done'
             elif(pos=='done' and k[i] =='('):
                pos = 'donev2'
             elif(pos=='donev2' and k[i]==')'):
                pos = 'done'
             elif((pos=='done' and not k[i].isnumeric())  ):
                pos = ''
                k+='**0.5'
                
             tmp+=k[i]

            if(pos=='done'):
              pos = ''
              tmp+='**0.5'

              tmp = tmp.replace('√','')
              k = tmp
          
          if('%' in k):
              k = k.replace('%','/100') 

          display.value = str(eval(k))

        elif(data=='AC'):
          display.value = '' 
        elif(data=='Del'):
          display.value = display.value[0:len(display.value)-1] 
        else:
          print(data,e)
          display.value += data 
          print(display.value)
        page.update()
    
    
    
    k17 = ElevatedButton("0",expand=1,on_click=partial(btn_click,'0'))
    k18 = ElevatedButton(".",on_click=partial(btn_click,'.'))
    k20 = ElevatedButton("=",bgcolor='orange',color='white',on_click=partial(btn_click,'='))

    k1 = ElevatedButton("1",on_click=partial(btn_click,'1'))
    k2 = ElevatedButton("2",on_click=partial(btn_click,'2'))
    k3 = ElevatedButton("3",on_click=partial(btn_click,'3'))
    k4 = ElevatedButton("-",bgcolor='orange',color='white',on_click=partial(btn_click,'-'))
    
      
    k5 = ElevatedButton("4",on_click=partial(btn_click,'4'))
    k6 = ElevatedButton("5",on_click=partial(btn_click,'5'))
    k7 = ElevatedButton("6",on_click=partial(btn_click,'6'))
    k8 = ElevatedButton("+",bgcolor='orange',color='white',on_click=partial(btn_click,'+'))
    k23 = ElevatedButton("π",bgcolor='orange',color='white',on_click=partial(btn_click,'π'))

    k9 = ElevatedButton("7",on_click=partial(btn_click,'7'))
    k10 = ElevatedButton("8",on_click=partial(btn_click,'8'))
    k11 = ElevatedButton("9",on_click=partial(btn_click,'9'))
    k12= ElevatedButton("×",bgcolor='orange',color='white',on_click=partial(btn_click,'×'))
    k22 = ElevatedButton(")",bgcolor='orange',color='white',on_click=partial(btn_click,')'))

    k13 = ElevatedButton("AC",bgcolor='red',color='white',on_click=partial(btn_click,'AC'))
    k24 = ElevatedButton("Del",bgcolor='red',color='white',on_click=partial(btn_click,'Del'))
    k14 = ElevatedButton("X²",bgcolor='orange',color='white',on_click=partial(btn_click,'²'))
    k15 = ElevatedButton("√",bgcolor='orange',color='white',on_click=partial(btn_click,'√'))
    k16= ElevatedButton("÷",bgcolor='orange',color='white',on_click=partial(btn_click,'÷'))
    k21 = ElevatedButton("(",bgcolor='orange',color='white',on_click=partial(btn_click,'('))
    
    display = TextField(value="", border_color='white',text_align="right",color='white',width=360)

    page.title = 'แบบฝึกหัดเครื่องคิดเลข'
    page.add(Container(width=360,
                       content=Column([Row([display]),
                                       Row([k13,k24,k15,k16,k21]),
                                       Row([k9,k10,k11,k12,k22]),
                                       Row([k5,k6,k7,k8,k23]),
                                       Row([k1,k2,k3,k4,k14]),
                                       Row([k17,k18,k20])
                                       ])))
   
    
flet.app(target=main)