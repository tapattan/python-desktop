# แบบฝึกหัด flet 
# https://flet.dev/docs/guides/python/getting-started
# flet lab01.py -d

import flet as ft

def main(page: ft.Page):
    def add_clicked(e):
        #tasks_view.controls.append(ft.Checkbox(label=new_task.value))
        new_task.value = ""
        
        try:
          for i in page:
            print(i)
        except:
          print(2)
          pass

        # table
        rowsK = []
        for i in range(35):
             rowsK.append(
                ft.DataRow(
                    cells=[
                        ft.DataCell(ft.Text('ccc')),
                    ],
                ))
         
        
        table = ft.DataTable(
            border=ft.border.all(1, 'black'),
            show_bottom_border=True,
            columns=[
                ft.DataColumn(ft.Text('ผลลัพธ์การค้นหา')),
            ],
            rows=rowsK, 
        )
        
        # end table 

        lv = ft.ListView(expand=1, spacing=10, padding=20, auto_scroll=True)
        lv.controls.append(table)

        #tasks_view.controls.append(lv)
        page.add(lv)
        view.update()

    new_task = ft.TextField(hint_text="ลองหาสิ่งที่คุณสนใจ", expand=True)
    tasks_view = ft.Column()
    view=ft.Column(
        width=600,
        controls=[
            ft.Row(
                controls=[
                    new_task,
                    ft.ElevatedButton("Done", on_click=add_clicked),
                ],
            ),
            tasks_view,
        ],
    )


    

    page.horizontal_alignment = ft.CrossAxisAlignment.CENTER
    page.add(view)

ft.app(target=main)

 