# run : flet chatStock2.py -d
import starfishX as sx
import flet as ft

def main(page: ft.Page):
    def add_clicked(e):
        
        #clear table
        for i in range(len(table.rows)):
          table.rows.pop(0)

        m = new_task.value 
        df = sx.news_api([m],highlight=False)
        df = df.sort_values('Date',ascending=False).head(20)

        for i in zip(df['Date'],df['contentShort']):
          k = str(i[0])[0:10] + ' '+i[1]
          b=ft.DataRow(
                cells=[
                    ft.DataCell(ft.Text(k)),
                    ])

          table.rows.append(b)

        page.update()
    
    
    rowsK = []
    for i in range(0):
             rowsK.append(
                ft.DataRow(
                    cells=[
                        ft.DataCell(ft.Text('test')),
                    ],
                ))

    table = ft.DataTable(
            border=ft.border.all(1, 'black'),
            show_bottom_border=True,
            columns=[
                ft.DataColumn(ft.Text('ผลลัพธ์การค้นหา')),
            ],
            #rows=rowsK,
     
        )
        
    # end table 

    lv = ft.ListView(expand=1, spacing=10, padding=0, auto_scroll=True)
    lv.controls.append(table)

    #tasks_view.controls.append(lv)
    #page.add(lv)

    new_task = ft.TextField(hint_text="ลองหาสิ่งที่คุณสนใจ", expand=True)
    tasks_view = ft.Column()
    view=ft.Column(
        width=600,
        controls=[
            ft.Row(
                controls=[
                    new_task,
                    ft.ElevatedButton("Done", on_click=add_clicked),
                ],
            ),
            tasks_view,
        ],
    )


    

    page.horizontal_alignment = ft.CrossAxisAlignment.CENTER
    page.title = 'แบบฝึกหัด Application ของชั้น'
    page.add(view)
    page.add(lv)
    


ft.app(target=main,
    assets_dir="assets",
)
