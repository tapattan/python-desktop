import flet
from flet import ElevatedButton, SnackBar, Text

class Data:
    def __init__(self) -> None:
        self.counter = 0

d = Data()

def main(page):

    page.snack_bar = SnackBar(
        content=Text("Hello, world!"),
        action="Alright!",
    )

    def on_click(e):
        page.snack_bar = SnackBar(Text(f"Hello {d.counter}"))
        page.snack_bar.open = True
        d.counter += 1
        page.update()

    page.add(ElevatedButton("Open SnackBar", on_click=on_click))

flet.app(target=main)