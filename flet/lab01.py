import flet
from flet import Text,TextField,ElevatedButton

# แบบฝึกหัด flet 
# https://flet.dev/docs/guides/python/getting-started
# flet lab01.py -d

def main(page):

    first_name = TextField(label='First name', autofocus=True)
    last_name = TextField(label='Last name')
    LabelDisplay = Text(value='', color='green')

    def btn_click(e):
        LabelDisplay.value = 'Hello '+first_name.value +' '+ last_name.value
        first_name.value = ''
        last_name.value = ''
        page.update()
        first_name.focus()

    page.add(
        LabelDisplay,
        first_name,
        last_name,
        ElevatedButton('Say hello!', on_click=btn_click),
    )


flet.app(target=main,view=flet.WEB_BROWSER)