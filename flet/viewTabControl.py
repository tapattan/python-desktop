from flet import Container, View,Icon,Ref,TextField, Tab, Tabs, Text,ElevatedButton, alignment, icons,GridView,Row,Column

from StockControl import StockControl 
from mainviewControl import mainviewControl
import random 
import datetime
#from faker import Faker

import pandas as pd

def viewTabControl(page):
    
    welcomeText2 = Text(
    value="Portfolio",
    size=20,
    color="pink",
    #bgcolor="pink",
    weight="bold",
    italic=True,)

    disPlayGrid = GridView(
                expand=1,
                runs_count=4,
                max_extent=120,
                child_aspect_ratio=1.0,
                spacing=10,
                run_spacing=10,
                padding=10,
                #height=100
            )

    #fake = Faker(['th-TH','en-US'])
    
    welcomeText1 = Text(
    value="Add New Stock",
    size=20,
    color="pink",
    #bgcolor="pink",
    weight="bold",
    italic=True,)

    #symbol = ['kce','chg','or','ori','mc','intuch','spali','sft','bis']

    
    try:
      df = pd.read_csv('fav.csv')
      df = df.sort_index(ascending=False)
      symbol = df['symbol'].values.tolist()
      for i in range(len(symbol)):
            #symbol = fake['en-US'].cryptocurrency_code()
            #d = fake['en-US'].date_between(start_date=datetime.date(2022,7,1))
            disPlayGrid.controls.append(
                StockControl(symbol[i])
            )
    except:
      symbol = []

    txtAddSymbol = TextField(label='First name',value='')

    def addSymbol_click(e):
        s = txtAddSymbol.value
        txtAddSymbol.value = ''
        try:
          df = pd.read_csv('fav.csv')
          df = df[['symbol']]
          t = pd.DataFrame({'symbol':s},index=[max(df.index)+1])
          df = df.append(t)
          df.to_csv('fav.csv')
          print('m',df)
        except:
          t = pd.DataFrame({'symbol':s},index=[0])
          t.to_csv('fav.csv') 
        
        disPlayGrid.controls.insert(0,
                    StockControl(s)
                )

        page.update()


    t = Tabs(
        selected_index=0,
        animation_duration=300,
        tabs=[
            Tab(
                text="Add Symbol",
                content=Container(
                    content=Column([welcomeText1,
                                Row([txtAddSymbol,
                                     ElevatedButton("add",on_click=addSymbol_click)])] ), 
                ),
            ),
            Tab(
                text="View Port",
                content=Column([welcomeText2,disPlayGrid])

            ),
            
        ],
        expand=1,
    )
    
    return View(
                    "/store",
                    [   
                        t,
                    ],
                    )