import flet
from flet import  Text,ElevatedButton, Row ,UserControl

from functools import partial

class Calculator(UserControl):
    def btn_click(self,data,e):
        print(data,e)
        self.update()

    def build(self):
     
        k1 = ElevatedButton("Add",on_click=partial(self.btn_click,'1'))
        k2 = ElevatedButton("Sub",on_click=partial(self.btn_click,'2'))
        return Row([k1,k2])

def main(page):
    page.add(Calculator())

flet.app(target=main)