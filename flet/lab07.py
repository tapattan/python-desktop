import flet
from flet import Page, Image

def main(page: Page):
    page.add(Image(src=f'images/sun.png',
                width=100,
                height=100,))

flet.app(
    target=main,
    assets_dir="assets"
)