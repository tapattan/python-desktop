import flet
from flet import AlertDialog,Icon, theme,border_radius,Banner,FilledButton, Page,Row,Column, TextField,Text, TextButton,Image,Container,colors,padding,icons

from viewTabControl import viewTabControl 

MAX_WIDTH = 500
MAX_HEIGHT = 500

def main(page: Page):
    page.title = "Member Club"
    page.theme_mode = "light"
    page.padding = 0 #ให้รูปด้านหลังเต็ม
    bg = Image(src='images/bg_green.png',
                fit="fill",
                repeat="repeat",
                width=MAX_WIDTH,
                height=MAX_HEIGHT,)
    
    def close_dlg(e):
        dlg_modal.open = False
        page.update()


    dlg_modal = AlertDialog(
        modal=True,
        title=Text("Please confirm"),
        content=Text("Do you really want to delete all those files?"),
        actions=[
            TextButton("Yes", on_click=close_dlg),
            TextButton("No", on_click=close_dlg),
        ],
        actions_alignment="end",
        on_dismiss=lambda e: print("Modal dialog dismissed!"),
    )
    
    def close_banner(e):
        page.banner.open = False
        page.update()

    page.banner = Banner(
        bgcolor=colors.AMBER_100,
        leading=Icon(icons.WARNING_AMBER_ROUNDED, color=colors.AMBER, size=40),
        content=Text(
            "Oops, Login Failed!\nYour username or password is incorrect.",size=10,
        ),
        actions=[
            #TextButton("Retry", on_click=close_banner),
            #TextButton("Ignore", on_click=close_banner),
            TextButton("Cancel", on_click=close_banner),
        ],
    )


    def view_pop(view):
        page.views.pop()
        top_view = page.views[-1]
        page.go(top_view.route)

    

    def button_clicked(e):
        u = txtUsername.value
        p = txtPassword.value
        txtUsername.value='' 
        txtPassword.value=''
        if(u=='' and p==''):
          return 0
        elif(u=='admin' and p=='1234'):
           pass 
           
           '''disPlayGrid = GridView(
                expand=1,
                runs_count=5,
                max_extent=150,
                child_aspect_ratio=1.0,
                spacing=5,
                run_spacing=5,
                #width=500
            )

    

           fake = Faker(['th-TH','en-US'])

           for i in range(0, 50):
                symbol = fake['en-US'].cryptocurrency_code()
                d = fake['en-US'].date_between(start_date=datetime.date(2022,7,1))
                disPlayGrid.controls.append(
                    GreeterControl(symbol,fake.name(),d)
                )

           page.views.append(mainviewControl(welcomeText,disPlayGrid))'''
           page.views.append(viewTabControl(page))
           page.theme = theme.Theme(color_scheme_seed="green")
           page.update()

        else:
           page.banner.open = True
           page.update()
    
    logoUsername = Container(
                content=Row([Image(
                    src=f"images/cat.jpg",
                    width=50,
                    height=50,
                    fit="fill",
                    border_radius=border_radius.all(45))] ,
                alignment='center',
                ),
                padding=padding.only(top=40)
                )

    # font ของ icons https://fonts.google.com/icons?icon.query=user&icon.set=Material+Icons&icon.platform=flutter
    txtUsername = TextField(value="",label="UserName",prefix_icon=icons.PERSON,border_color='white',text_align="left",color='white',width=200,height=30,text_size=10)
    txtPassword = TextField(value="",label="Password",prefix_icon=icons.PASSWORD, 
                            password=True, can_reveal_password=True,border_color='white',text_align="left",color='white',width=200,height=30,text_size=10)
    btnLogin = FilledButton("LOGIN", icon="login",icon_color='WHITE',width=200,height=30,on_click=button_clicked)
    
    #[txtUsername,txtPassword,btnLogin]
    btnLoginContent = Container(
                content=Column( [Row([logoUsername],alignment='center'),
                                 Row([Text(value='',size=50)]),
                                 Row([txtUsername],alignment='center'),
                                 Row([txtPassword],alignment='center'),
                                 Row([btnLogin],alignment='center'),
                                 Row([Text(value='',size=500)]) 
                                 ] ,
                                 
                        alignment='center',),
                image_src=f"images/bg_green.png",
                image_fit='fill',
                image_repeat='repeat',
                padding=padding.only(top=0),
                width=MAX_WIDTH,
                height=MAX_HEIGHT,
                )
    '''st = Stack(
        [
          Container(
                content=Row([bg]),padding=padding.only(top=0)),logoUsername,btnLoginContent
        ],
        width=800,
        height=800,
    )'''
     
    page.add(btnLoginContent)

    ###############
    
    
    page.window_width = MAX_WIDTH
    page.window_height = MAX_HEIGHT
    page.window_max_width = MAX_WIDTH
    page.window_min_width = MAX_WIDTH
    page.window_max_height = MAX_HEIGHT
    page.window_min_height = MAX_HEIGHT
    page.update()
flet.app(target=main,assets_dir="assets",)