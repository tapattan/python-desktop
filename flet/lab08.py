import flet
from flet import Page,Container, Image ,Row,Column,Text,colors ,IconButton,icons

from functools import partial

def main(page: Page):
    
    indexLastImage = 3
   
    indexAt = Text(value="0") #เอาไว้ทำสมุดจด
    imgList = ['apple.png','banana.png','coconut.png','orange.png'
              ,'pineapple.png','strawberry.png','tomato.png','durian.png','lime.png','cherry.png','mango.png','mangosteen.png']
    imgSelectd = []
    for i in range(0,indexLastImage):
       m1 = Image(src='images/'+imgList[i],
                width=100,
                height=100,)
       imgSelectd.append(m1)

    c = Column(
            [
                Text('album ผลไม้', size=10),
                Container(
                    content=Row(imgSelectd, alignment='center'),
                    bgcolor=colors.TRANSPARENT,
                ),
            ]
        )

    def button_clicked(k,e):
        tmp = int(indexAt.value)+k
         
        if(tmp>=len(imgList)-3):
           tmp = len(imgList)-3
        elif(tmp==-1):
           tmp = 0
        indexAt.value = str(tmp) 
        
        if(tmp==0):
           b1.disabled=True
        else:
           b1.disabled=False
        
        if(tmp==len(imgList)-3):
           b2.disabled=True
        else:
           b2.disabled=False
        
        for i in range(3):
            imgSelectd[i] = Image(src='images/'+imgList[i+tmp],
                width=100,
                height=100,)

        page.update()
    

    # https://fonts.google.com/icons?selected=Material+Icons&icon.platform=flutter
    b1 = IconButton(
        icon=icons.CHEVRON_LEFT, on_click=partial(button_clicked,-1), data=0,disabled = True,
    )
    b2 = IconButton(
        icon=icons.CHEVRON_RIGHT, on_click=partial(button_clicked,1), data=0
    )
    c1 = Column( 
            [
                Container(
                    content=Row([b1,b2], alignment='center'),
                    bgcolor=colors.TRANSPARENT,
                ),
            ]
        )
    
   
    page.add(c)
    page.add(c1)

    # https://flet.dev/docs/controls/page
    page.window_width = 400
    page.window_height = 400
    page.window_max_width = 400
    page.window_min_width = 400
    page.window_max_height = 400
    page.window_min_height = 400
    page.update()

flet.app(
    target=main,
    assets_dir="assets",
)



 