from turtle import bgcolor, width
import flet
from flet import Image, Page, GridView,Container, border_radius ,Text ,ElevatedButton,TextButton,UserControl,Column,Row,ButtonStyle,theme,padding
from flet.buttons import RoundedRectangleBorder

import urllib.request, json 
import time 
class StockControl(UserControl):
    def __init__(self, initial_symbol):
        super().__init__()
        self.symbol = initial_symbol.upper()
        self.price = 0
        self.datepost = ''
        self.url = 'https://www.set.or.th/api/set/stock/'+self.symbol+'/related-product/o'

   

    def getPrice(self):
        with urllib.request.urlopen(self.url) as url:
             data = json.load(url)

        return [str(data['relatedProducts'][0]['last']),str(data['relatedProducts'][0]['sign']),str(round(data['relatedProducts'][0]['percentChange'],2)),  ]   

    def build(self):
        rp = self.symbol 
        self.price,CA,percentChange = self.getPrice()
        color = 'blue'
        if(float(percentChange)<0):
           color = 'red'
        elif(float(percentChange)>0):
           color = 'green' 
        
        if(CA!=''):
           CA = ' ('+CA+')' 
        return Container(
            content=Column([ElevatedButton(
                      content=Container(
                          content=Column(
                           [
                            Text(value=self.symbol+CA, size=15),
                            Text(value='Price:'+self.price,size=8,color=color),
                            Text(value='('+percentChange+'%)',color=color,size=8),
                           ],
                           alignment="center",
                           #spacing=5,
                           width=120,
                           ),
                padding=padding.all(5),
                ), style=ButtonStyle( shape={
                    "hovered": RoundedRectangleBorder(radius=0),
                    "": RoundedRectangleBorder(radius=0),
                } )
            ),]) ,
            bgcolor='white',height=50,width=120)