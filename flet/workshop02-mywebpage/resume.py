import flet as ft

def main(page: ft.Page):
    t = ft.Text(value="ประวัติของชั้น", color="green")
    page.controls.append(t)
    page.update()

    img = ft.Image(
        src=f"images/profile.png",
        width=100,
        height=100,
        fit=ft.ImageFit.CONTAIN,
    )
    images = ft.Row(expand=1, wrap=False, scroll="always")

    page.controls.append(img)
    

    table = ft.DataTable(
            columns=[
                ft.DataColumn(ft.Text("ข้อมูล")),
                ft.DataColumn(ft.Text("รายละเอียด")),
            ],
            rows=[
                ft.DataRow(
                    cells=[
                        ft.DataCell(ft.Text("ชื่อ")),
                        ft.DataCell(ft.Text("Smith")),
                    ],
                ),
                ft.DataRow(
                    cells=[
                        ft.DataCell(ft.Text("นามสกุล")),
                        ft.DataCell(ft.Text("Brown")),
                    ],
                ),
                ft.DataRow(
                    cells=[
                        ft.DataCell(ft.Text("อายุ")),
                        ft.DataCell(ft.Text("25")),
                    ],
                ),
            ],
        )

    page.add(table)

    page.update()

ft.app(target=main,view=ft.WEB_BROWSER,assets_dir='assets')