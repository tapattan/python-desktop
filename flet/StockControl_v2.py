import flet
from flet import Image, Page, GridView,Container, border_radius ,Text ,ElevatedButton,TextButton,UserControl,Column,Row,ButtonStyle,theme,padding
from flet.buttons import RoundedRectangleBorder

import urllib.request, json 

import threading
import time
from flet.ref import Ref
import random 

from tvDatafeed import TvDatafeed, Interval

class StockControl(UserControl):
    def __init__(self, initial_symbol):
        super().__init__()
        self.symbol = initial_symbol.upper()
        self.price = 0
        self.volume = 0
        self.datepost = ''
        #self.url = 'https://www.set.or.th/api/set/stock/'+self.symbol+'/related-product/o'
        self.url = 'https://www.settrade.com/api/set/stock/'+self.symbol+'/info'
        
        self.cnt = 0 
        self.labelStock = Ref[Text]()
        self.labelVolume = Ref[Text]()

    def auto_refresh_page(self,s):
        while(True):
          print("Thread starting", self.symbol,self.cnt)
          self.cnt += s
          with urllib.request.urlopen(self.url) as url:
             data = json.load(url)

          #self.price = 'Price:'+str(data['relatedProducts'][0]['last'])+ ' ('+str(round(data['relatedProducts'][0]['percentChange'],2))+'%)'
          #my_string = '{:0,.0f}'.format(data['relatedProducts'][0]['totalVolume'])

          self.price = 'Price:'+str(data['last'])+ ' ('+str(round(data['percentChange'],2))+'%)'
          my_string = '{:0,.0f}'.format(data['totalVolume'])

          self.volume = 'Volume:'+str(my_string)
          print('updating',self.price,self.volume)

          time.sleep(2)

    def getPrice(self):
        with urllib.request.urlopen(self.url) as url:
             data = json.load(url)
        
        return [str(data['last']),
                str(data['sign']),
                str(round(data['percentChange'],2)), ]
        '''return [str(data['relatedProducts'][0]['last']),
                str(data['relatedProducts'][0]['sign']),
                str(round(data['relatedProducts'][0]['percentChange'],2)),  ]   '''

    def build(self):
        rp = self.symbol 
        self.price,CA,percentChange = self.getPrice()
        color = 'blue'
        if(float(percentChange)<0):
           color = 'red'
        elif(float(percentChange)>0):
           color = 'green' 
        
        if(CA!=''):
           CA = ' ('+CA+')' 
        return Container(
            content=Column([ElevatedButton(
                      content=Container(
                          content=Column(
                           [
                            Text(value=rp+' '+CA, size=14),
                            Text(value='',size=6,color=color,ref=self.labelStock),
                            Text(value='',color=color,size=6,ref=self.labelVolume),
                           ],
                           alignment="center",
                           #spacing=5, 
                           width=130,
                           ),
                padding=padding.all(5),
                ), style=ButtonStyle( shape={
                    "hovered": RoundedRectangleBorder(radius=0),
                    "": RoundedRectangleBorder(radius=0),
                } )
            ),]) ,
            bgcolor='white',height=40,width=130)