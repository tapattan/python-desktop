from turtle import bgcolor, width
import flet
from flet import Image, Page, GridView,Container, border_radius ,Text ,ElevatedButton,TextButton,UserControl,Column,ButtonStyle,theme
from flet.buttons import RoundedRectangleBorder

import random 
import datetime
from faker import Faker

class GreeterControl(UserControl):
    def __init__(self, initial_symbol,who_post,datepost):
        super().__init__()
        self.symbol = initial_symbol
        self.poster = who_post
        self.datepost = str(datepost)
    def build(self):
        return Container(
            content=Column([
             ElevatedButton(self.symbol,width=150,height=50,bgcolor='white', style=ButtonStyle( shape={
                    "hovered": RoundedRectangleBorder(radius=0),
                    "": RoundedRectangleBorder(radius=0),
                } ) ),
             Text(value='by '+self.poster,size=10),   
             Text(value='update:'+self.datepost,size=10),
         ]) ,bgcolor='white',height=50,width=120)

a2z = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
def randsymbol():
    k = random.randint(1,5)
    p = random.choices(a2z,k=k)
    return ''.join(p)

def main(page: Page):
    page.title = "แบบฝึกหัด GridView"
    #page.theme_mode = 'dark'
    page.theme = theme.Theme(color_scheme_seed="green")
    page.padding = 50
    page.update()
 
    disPlayGrid = GridView(
        expand=1,
        runs_count=5,
        max_extent=150,
        child_aspect_ratio=1.0,
        spacing=5,
        run_spacing=5,
        #width=500
    )

    page.add(disPlayGrid)

    fake = Faker(['th-TH','en-US'])

    for i in range(0, 50):
        symbol = fake['en-US'].cryptocurrency_code()
        d = fake['en-US'].date_between(start_date=datetime.date(2022,7,1))
        disPlayGrid.controls.append(
            GreeterControl(symbol,fake.name(),d)
        )
    page.update()

flet.app(target=main, view=flet.WEB_BROWSER)