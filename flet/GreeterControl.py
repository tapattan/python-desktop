from turtle import bgcolor, width
import flet
from flet import Image, Page, GridView,Container, border_radius ,Text ,ElevatedButton,TextButton,UserControl,Column,ButtonStyle,theme
from flet.buttons import RoundedRectangleBorder

class GreeterControl(UserControl):
    def __init__(self, initial_symbol,who_post,datepost):
        super().__init__()
        self.symbol = initial_symbol
        self.poster = who_post
        self.datepost = str(datepost)
    def build(self):
        return Container(
            content=Column([
             ElevatedButton(self.symbol,width=150,height=50,bgcolor='white', style=ButtonStyle( shape={
                    "hovered": RoundedRectangleBorder(radius=0),
                    "": RoundedRectangleBorder(radius=0),
                } ) ),
             Text(value='by '+self.poster,size=10),   
             Text(value='update:'+self.datepost,size=10),
         ]) ,bgcolor='white',height=50,width=120)