

#pip install tksheet 

import tkinter as tk

from functools import partial
from tksheet import Sheet

def ref():
        #https://snyk.io/advisor/python/tksheet/example
        
        #https://www.activestate.com/resources/quick-reads/how-to-display-data-in-a-table-using-tkinter/
        #https://github.com/ragardner/tksheet/wiki#13-row-heights-and-column-widths
        #https://github.com/ragardner/tksheet/blob/master/DOCUMENTATION.md
        pass

class demo(tk.Tk):
 
    def btndone_onclick(self,k):
        c1 = self.txtbox1.get()
        c2 = self.txtbox2.get()
        c3 = self.txtbox3.get()
        c4 =self. txtbox4.get()
        self.sheet.insert_row(values=[c1,c2,c3,c4],redraw = True) 
        self.txtbox1.delete(0,tk.END)
        self.txtbox2.delete(0,tk.END)
        self.txtbox3.delete(0,tk.END)
        self.txtbox4.delete(0,tk.END)
       

    def __init__(self):
        tk.Tk.__init__(self)
        
        self.title('Hello world')

        #เพิ่มปุ่มเข้ามา
        button = tk.Button(self,text='done!', command=partial(self.btndone_onclick,'p'))
        button.place(x=20,y=90) #replace values with required
        
        lbname = tk.Label(self,text='ชื่อ-นามสกุล')
        lbname.place(x=20,y=5)
        self.txtbox1 = tk.Entry(self,width=10)
        self.txtbox1.place(x=100,y=5)

        lbname = tk.Label(self,text='email')
        lbname.place(x=220,y=5)
        self.txtbox2 = tk.Entry(self,width=12)
        self.txtbox2.place(x=280,y=5)

        lbname = tk.Label(self,text='ที่อยู่')
        lbname.place(x=20,y=40)
        self.txtbox3 = tk.Entry(self,width=10)
        self.txtbox3.place(x=100,y=40)

        lbname = tk.Label(self,text='เบอร์โทร')
        lbname.place(x=220,y=40)
        self.txtbox4 = tk.Entry(self,width=12)
        self.txtbox4.place(x=280,y=40)
        
        data = [] 
        for row in range(2):#row
            _ = []
            for col in range(4):
                _.append('data'+str(row)+','+str(col)) 
            data.append(_) 

        #[[f"Row {r}, Column {c}\nnewline1\nnewline2" for c in range(4)] for r in range(5)])
        self.sheet = Sheet(self,column_width = 80,width=380,height=200,show_y_scrollbar = True,show_x_scrollbar = False,
                    data = data)
        
        #sheet.grid()
        #sheet.change_theme("light green")
        self.sheet.enable_bindings(("rc_insert_row","edit_cell","column_width_resize"))
        self.sheet.place(x=20,y=140)

        #self.sheet.insert_row() 

        self.geometry("500x500")
 

app = demo()
app.mainloop()