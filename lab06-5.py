def print_something(k):

    #set/get to label
    v = label1.cget("text")
    if(k=='U'):
      v = int(v)+1 
    else:
      v = int(v)-1

    label1.config(text=v)
       

import tkinter as tk
from functools import partial

app = tk.Tk()
app.title('แบบฝึกหัด : โปรแกรม นับเลข')

# label 
label0 = tk.Label(app,text='Count : ',width=6,borderwidth=1,relief='solid')
label0.place(x=25,y=0)

label1 = tk.Label(app,text='0',width=2,borderwidth=1)
label1.place(x=100,y=0)

#เพิ่มปุ่มเข้ามา
button = tk.Button(app,text='UP!🔼', command=partial(print_something,'U'))
button.place(x=20,y=50) #replace values with required
#button.pack()  #comment this

#เพิ่มปุ่มเข้ามา
button = tk.Button(app,text='DOWN!🔽', command=partial(print_something,'D'))
button.place(x=100,y=50) #replace values with required


app.geometry("300x300")
app.mainloop()

