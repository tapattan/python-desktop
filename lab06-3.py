def calculator(k):
    if(k == 'a'):
        k = textbox1.get() # get value from textbox
        k2 = textbox2.get() # get value from textbox
        total = float(k) + float(k2)
        textboxans.delete(0,tk.END) # clear value of textbox
        textboxans.insert(tk.END,total)
        print(total)
    elif(k == 's'):
        k = textbox1.get() # get value from textbox
        k2 = textbox2.get() # get value from textbox
        total = float(k) - float(k2)
        textboxans.delete(0,tk.END) # clear value of textbox
        textboxans.insert(tk.END,total)
        print(total)
    elif(k == 'm'):
        k = textbox1.get() # get value from textbox
        k2 = textbox2.get() # get value from textbox
        total = float(k) * float(k2)
        textboxans.delete(0,tk.END) # clear value of textbox
        textboxans.insert(tk.END,total)
        print(total)
    elif(k == 'd'):
        k = textbox1.get() # get value from textbox
        k2 = textbox2.get() # get value from textbox
        total = float(k) / float(k2)
        textboxans.delete(0,tk.END) # clear value of textbox
        textboxans.insert(tk.END,total)
        print(total)


import tkinter as tk
from functools import partial

app = tk.Tk()
app.title('Calculator')

#add more button
buttona = tk.Button(app,text='  +  ', command=partial(calculator,'a'))
buttona.place(x=40,y=200) #replace values with required
#button.pack()  #comment this

buttons = tk.Button(app,text='  -  ', command=partial(calculator,'s'))
buttons.place(x=80,y=200)

buttonm = tk.Button(app,text='  *  ', command=partial(calculator,'m'))
buttonm.place(x=120,y=200)

buttond = tk.Button(app,text='  /  ', command=partial(calculator,'d'))
buttond.place(x=160,y=200)

textbox1 = tk.Entry()
textbox1.place(x=40,y=50)
textbox1.insert(tk.END,'0')
textbox1.config(font =("Courier", 14))
#textbox.pack()  

textbox2 = tk.Entry()
textbox2.place(x=40,y=100)
textbox2.insert(tk.END,'0')
textbox2.config(font =("Courier", 14))

textboxans = tk.Entry()
textboxans.place(x=40,y=150)
textboxans.insert(tk.END,'0')
textboxans.config(font =("Courier", 14))

app.geometry("300x300")
app.mainloop()