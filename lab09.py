def print_something():
    print('Hi..')
    
import tkinter as tk
from tkinter import Checkbutton

app = tk.Tk()
app.title('Hello world')

#เพิ่มปุ่มเข้ามา
label1 = tk.Label(app,text='งานอดิเรกของคุณ : ',width=12)
#label1.place(x=0,y=0)
label1.grid(row=0,column=0,sticky='e')
 
 
Checkbutton1 = Checkbutton(app, text="ดูหนัง",
                           onvalue=1, offvalue=0).grid(row=1,column=1,sticky='w')
Checkbutton2 = Checkbutton(app, text="เล่นกีฬา",
                           onvalue=1, offvalue=0).grid(row=2,column=1,sticky='w')
Checkbutton3 = Checkbutton(app, text="ปลูกต้นไม้",
                           onvalue=1, offvalue=0).grid(row=3,column=1,sticky='w')

button = tk.Button(app,text='click me!',bg = "yellow", command=print_something)
#button.place(x=50,y=0) #replace values with required
#button.pack()  #comment this
button.grid(row=4,column=1,sticky='w')

app.geometry("300x300")
app.mainloop()