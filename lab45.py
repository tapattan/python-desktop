# Python program to create a table
# ตัวอย่างการใช้ tableview ด้วย entry  
from tkinter import *
import tkinter as tk
 
class Table:
     
    def __init__(self,root):
         
        # code for creating table
        for i in range(total_rows):
            for j in range(total_columns):

                if(j==0 or j==3): #กำหนดให้ 0 คือ columns แรก
                  w_ = 3
                elif(j==4):
                  w_ = 25    
                else:
                  w_ = 10   

                self.e = Entry(root, width=w_, fg='black',
                               font=('Arial',10,'bold'))
                 
                self.e.grid(row=i, column=j)
                self.e.insert(END, lst[i][j])
 
# take the data
lst = [(1,'Raj','Mumbai',19,'Raj.Mumbai@gmail.com'),
       (2,'Aaryan','Pune',18,'Aaryan.Pune@gmail.com'),
       (3,'Vaishnavi','Mumbai',20,'Vaishnavi.Mumbai@gmail.com'),
       (4,'Rachna','Mumbai',21,'Rachna.Mumbai@gmail.com'),
       (5,'Shubham','Delhi',21,'Shubham.Delhi@gmail.com')]
  
# find total number of rows and
# columns in list
total_rows = len(lst)
total_columns = len(lst[0])  
  

  
# create root window
app = Tk()
container01 = tk.Frame(app,height=200,width=500,bg='black')
container01.place(x=20,y=20)

t = Table(container01)

app.geometry('600x300')
app.mainloop()