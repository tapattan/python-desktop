def print_something(k):
    print('Hi..' +str(k))
    textbox.delete(0,tk.END)
    textbox.insert(tk.END,'Hi..'+str(k))

import tkinter as tk
from functools import partial

app = tk.Tk()
app.title('Hello world')

#เพิ่มปุ่มเข้ามา
label1 = tk.Label(app,text='ชื่อ : ',width=3)
#label1.place(x=0,y=0)
label1.grid(row=0,column=0,sticky='e')

button = tk.Button(app,text='click me!',bg = "yellow", command=partial(print_something,'David'))
#button.place(x=50,y=0) #replace values with required
#button.pack()  #comment this
button.grid(row=0,column=1,sticky='w')


label2 = tk.Label(app,text='',width=3)
#label1.place(x=0,y=0)
label2.grid(row=1,column=0)

textbox = tk.Entry()
#textbox.place(x=50,y=50)
textbox.config(font =("Courier", 14))
#textbox.pack()  
textbox.grid(row=1,column=1,sticky='w')

app.geometry("300x300")
app.mainloop()


