import tkinter as tk
from tkinter import ttk
from tkinter.messagebox import showinfo
from calendar import month_name

app = tk.Tk()
app.title('Combobox Widget')

# label
label = ttk.Label(text="Please select a month:")
label.grid(row=0,column=0,sticky='w')

# create a combobox
selected_month = tk.StringVar()
month_cb = ttk.Combobox(app, textvariable=selected_month)

# get first 3 letters of every month name
month_cb['values'] = [month_name[m] for m in range(1, 13)]

# prevent typing a value
month_cb['state'] = 'readonly'

# place the widget
month_cb.grid(row=1,column=0)


# bind the selected value changes
def month_changed(event):
    """ handle the month changed event """
    showinfo(
        title='Result',
        message=f'You selected {selected_month.get()}!'
    )

month_cb.bind('<<ComboboxSelected>>', month_changed)


app.geometry('300x300')
app.resizable(True, True)
app.mainloop()