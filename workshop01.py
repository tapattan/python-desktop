#lab เครื่องคิดเลข
def btnHit(k):
    if(k=='='):
        try:
          data = display.cget("text")
          ans = eval(data)
          display.config(text=str(ans))
        except:
          display.config(text='error') 
    elif(k=='AC'):
        display.config(text='')   
    elif(k=='Del'):
        data = display.cget("text")  
        display.config(text=data[0:len(data)-1])
    else:
        data = display.cget("text")
        data = data+k
        display.config(text=data)
    
import tkinter as tk
from functools import partial
app = tk.Tk()
app.title('calculator')

display = tk.Label(app,text='',highlightthickness=1,borderwidth=1, relief="groove",width=32 , anchor="e", justify=tk.LEFT)
display.grid(row=0,column=0,sticky='ns',columnspan=294)

button1 = tk.Button(text='1',width=5,height=2, command=partial(btnHit,'1'))
button1.grid(row=1,column=0,sticky='w')
button2 = tk.Button(text='2',width=5,height=2, command=partial(btnHit,'2'))
button2.grid(row=1,column=1,sticky='w')
button3 = tk.Button(text='3',width=5,height=2 ,command=partial(btnHit,'3'))
button3.grid(row=1,column=2,sticky='w')
button4 = tk.Button(text='+',width=5,height=2, command=partial(btnHit,'+'))
button4.grid(row=1,column=3,sticky='w')

button5 = tk.Button(text='4',width=5,height=2, command=partial(btnHit,'4'))
button5.grid(row=2,column=0,sticky='w')
button6 = tk.Button(text='5',width=5,height=2, command=partial(btnHit,'5'))
button6.grid(row=2,column=1,sticky='w')
button7 = tk.Button(text='6',width=5,height=2, command=partial(btnHit,'6'))
button7.grid(row=2,column=2,sticky='w')
button8 = tk.Button(text='-',width=5,height=2, command=partial(btnHit,'-'))
button8.grid(row=2,column=3,sticky='w')

button9 = tk.Button(text='7',width=5,height=2, command=partial(btnHit,'7'))
button9.grid(row=3,column=0,sticky='w')
button10 = tk.Button(text='8',width=5,height=2, command=partial(btnHit,'8'))
button10.grid(row=3,column=1,sticky='w')
button11 = tk.Button(text='9',width=5,height=2, command=partial(btnHit,'9'))
button11.grid(row=3,column=2,sticky='w')
button12 = tk.Button(text='x',width=5,height=2, command=partial(btnHit,'*'))
button12.grid(row=3,column=3,sticky='w')

button13 = tk.Button(text='0',width=5,height=2, command=partial(btnHit,'0'))
button13.grid(row=4,column=0,sticky='w')
button14 = tk.Button(text='.',width=5,height=2, command=partial(btnHit,'.'))
button14.grid(row=4,column=1,sticky='w')
button15 = tk.Button(text='=',width=5, height=2,command=partial(btnHit,'='))
button15.grid(row=4,column=2,sticky='w')
button15 = tk.Button(text='/',width=5,height=2, command=partial(btnHit,'/'))
button15.grid(row=4,column=3,sticky='w')

button13 = tk.Button(text='(',width=5,height=2, command=partial(btnHit,'('))
button13.grid(row=5,column=0,sticky='w')
button14 = tk.Button(text=')',width=5,height=2, command=partial(btnHit,')'))
button14.grid(row=5,column=1,sticky='w')
button15 = tk.Button(text='Del',width=5, height=2,command=partial(btnHit,'Del'))
button15.grid(row=5,column=2,sticky='w')
button15 = tk.Button(text='AC',width=5,height=2, command=partial(btnHit,'AC'))
button15.grid(row=5,column=3,sticky='w')

app.geometry("294x294")
app.mainloop()