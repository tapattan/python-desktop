def print_something():
    print('Hi..'+str(w.get()))
    print('Hi..'+str(current_value.get()))
   
def scale_onchange(p): #event call on change
     print('onchange..'+str(current_value.get()))
     label2.config(text = str(current_value.get())+'%')
     
import tkinter as tk
from tkinter import Scale
from tkinter import HORIZONTAL
from tkinter import VERTICAL
from tkinter import DoubleVar
 

app = tk.Tk()
app.title('Hello world')

#เพิ่มปุ่มเข้ามา
label1 = tk.Label(app,text='อัตราการเติบโต : ',width=12)
#label1.place(x=0,y=0)
label1.grid(row=0,column=0,sticky='e')

current_value = DoubleVar()

w = Scale( app, from_=-100.0, to=100.0,resolution=1.0, variable = current_value ,
            orient=HORIZONTAL ,command=scale_onchange)
w.grid(row=0,column=1)

label2 = tk.Label(app,text='',width=12)
#label1.place(x=0,y=0)
label2.grid(row=1,column=1,sticky='e')

button = tk.Button(app,text='click me!',bg = "yellow", command=print_something)
#button.place(x=50,y=0) #replace values with required
#button.pack()  #comment this
button.grid(row=2,column=1,sticky='w')

app.geometry("300x300")
app.mainloop()