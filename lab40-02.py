def countdown():
    pass
    global gametime

    gametime = gametime - 1
    if(gametime <= 0):
        gametime = 0

    Label3.configure(text=str('Time : ') + str(gametime))
    Label3.after(1000, countdown) 

    
def startgame(a):
    pass
    global colours
    global tc
    global fc
    global gametime
    global score
    global afterRun

    if(gametime != 0):
        return 0
    gametime = 30
    score = 0
    Label2.configure(text=str('Score : ') + str(score)) 
    print(afterRun)

    fc = colours[random.randint(0,9)]
    tc = colours[random.randint(0,9)]
    if(a == 1 and afterRun == False): #ส่วนป้องกันไม่ให้เวลารัน speed เร็วขึ้นในรอบถัดไป
        score = 0
        #Label3.after(1000, countdown)
        countdown()
        afterRun = True
        
        Label4.configure(fg = str(fc), text = tc)


def Score(b):
    pass
    global gametime
    global score
    global tc
    global fc

    if(gametime <= 0):
        return 0
    g = Entry1.get()
    print(g,tc,fc)
    if(g==fc):
        score = score + 1
        Label2.configure(text=str('Score : ') + str(score))
    fc = colours[random.randint(0,9)]
    tc = colours[random.randint(0,9)]
    Label4.configure(fg = str(fc), text = tc)

from tkinter import *
import tkinter as tk
from functools import partial
import random

app = tk.Tk()
app.title('ColorGame')

colours = ['Red','Blue','Green','Pink','Black', 'Yellow','Orange','White','Purple','Brown']
tc = ''
fc = ''
gametime = 0
score = 0

afterRun = False #ส่วนป้องกันไม่ให้เวลารัน speed เร็วขึ้นในรอบถัดไป


Button1 = tk.Button(app,text='Start',bd=5,command=partial(startgame,1))
Button1.place(x=125,y=330)
Label1 = tk.Label(app,text='พิมพ์สีที่คุณเห็นในอักษรที่คุณเห็น')
Label1.place(x=60,y=10)
Label2 = tk.Label(app,text='Score : 0')
Label2.place(x=100,y=30)
Label3 = tk.Label(app,text='Time : 30')
Label3.place(x=100,y=50)
Label4 = tk.Label(app,text='',font = ('Helvetica', 50),width=10,height=3,borderwidth=2,relief='solid')
Label4.place(x=60,y=100)
Entry1 = Entry(app,bd=5,width=20)
Entry1.insert(0, "Type color here...")
Entry1.place(x=90,y=290)
Button2 = tk.Button(app,text='Guess',bd=5,command=partial(Score,1))
Button2.place(x=300,y=290)

app.resizable(False, False)
app.geometry("500x500")
app.mainloop()


