def button_callback():
    try:
        symbol = txtbox1.get()
        df = sx.loadHistData_v2(symbol,start='2022-01-01',end='2022-12-24')

        df = df[['Close','Volume']]
        df = df.reset_index()
        df['Date']=df['Date'].astype(str)
        df = df.values.tolist()
        
        sheet.set_sheet_data(data=df,redraw = True)
    except:
        messagebox.showwarning("show-warning", "ไม่พบข้อมูล")

import tkinter
import customtkinter
from tksheet import Sheet
from functools import partial
from tkinter import messagebox

import starfishX as sx 

customtkinter.set_appearance_mode('dark')  # Modes: "System" (standard), "Dark", "Light"
customtkinter.set_default_color_theme('blue')  # Themes: "blue" (standard), "green", "dark-blue"

app = customtkinter.CTk()
app.title('แบบฝึกหัดของชั้น')

frame_1 = customtkinter.CTkFrame(master=app,width=500,height=700)
frame_1.place(x=10,y=10)

label_1 = customtkinter.CTkLabel(master=frame_1,text='ข้อมูลหุ้น\n',font=('Courier bold', 12))
label_1.place(x=10, y=10)

button_1 = customtkinter.CTkButton(master=frame_1,text='done',font =('Courier', 10), command=button_callback,width=5)
button_1.place(x=170, y=30)

#entry
txtbox1 = customtkinter.CTkEntry(master=frame_1, placeholder_text='กรอกชื่อหุ้นที่คุณต้องการ',font =('Courier', 10)) 
txtbox1.place(x=10,y=30)

##### tableview
'''datasource = [['a','b','c'],['d','e','f']]
data = [] 
for row in datasource:#row
    _ = []
    for col in row:
      _.append('data'+str(col)) 
    data.append(_) '''

#[[f"Row {r}, Column {c}\nnewline1\nnewline2" for c in range(4)] for r in range(5)])
sheet = Sheet(frame_1,column_width = 80,width=380,height=600,show_y_scrollbar = True,show_x_scrollbar = False,
               #data = data) 
                )

sheet.change_theme(theme = 'dark blue')  #light blue, light green, dark, dark blue and dark green 
 
sheet.enable_bindings()
sheet.place(x=10,y=70)
########### end tableview

app.geometry('500x700')
app.mainloop()