def button_callback(event=None):
    print(event)
    #print("Button click", label_1.cget('text'))
    global focus
    if(focus==True):
      k = entry_1.get()
      label_1.configure(text='ยินดีต้อนรับ คุณ '+k+'\n')

def motion(event):
    #x, y = event.x, event.y
    #print('{}, {}'.format(x, y))
    print('motion')
    global focus
    if event.widget == entry_1:
        focus = True
        print("I have gained the focus")

import tkinter
import customtkinter as tk

tk.set_appearance_mode("dark")  # Modes: "System" (standard), "Dark", "Light"
tk.set_default_color_theme("blue")  # Themes: "blue" (standard), "green", "dark-blue"

app = tk.CTk()
app.title("แบบฝึกหัดของชั้น ")
focus = False 

frame_1 = tk.CTkFrame(master=app)
frame_1.place(x=10, y=10, width=300)

label_1 = tk.CTkLabel(master=frame_1,text='ยินดีต้อนรับ',height=5,font=("Courier", 14))
label_1.place(x=10, y=20)

entry_1 = tk.CTkEntry(master=frame_1, placeholder_text="กรุณากรอกชื่อของคุณ : ")
entry_1.place(x=10, y=50)

button_1 = tk.CTkButton(master=frame_1, text='click me!',command=button_callback)
button_1.place(x=10, y=90)
app.bind('<Return>', button_callback)

app.bind('<FocusIn>', motion)

app.geometry("400x400")
app.mainloop()