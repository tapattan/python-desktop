def calculator(k):
    if(k == 'p'):
        k = textbox1.get() # get value from textbox
        k2 = textbox2.get() # get value from textbox
        k3 = textbox0.get()
        total = (2 * float(k)) + (3 * float(k2)) +(1 * float(k3))
        textboxans.delete(0,tk.END) # clear value of textbox
        textboxans.insert(tk.END,total)
        print(total)


def save():
    pts2 = textbox1.get() # get value from textbox
    pts3 = textbox2.get() # get value from textbox
    pts1 = textbox0.get()
    total = (2 * float(pts2)) + (3 * float(pts3)) +(1 * float(pts1))
    date_post = textboxDate.get() 

    k = [date_post,pts3,pts2,pts1, str(total)]
    global datasource
    datasource.append(k) 


def refresh():
     
    for tabK in tab2.winfo_children():
        if not isinstance(tabK, tk.Button):
            tabK.destroy()

    game_scroll = Scrollbar(tab2)
    game_scroll.pack(side=RIGHT, fill=Y)

    game_scroll = Scrollbar(tab2,orient='horizontal')
    game_scroll.pack(side= BOTTOM,fill=X)

    my_game = ttk.Treeview(tab2,yscrollcommand=game_scroll.set, xscrollcommand =game_scroll.set)

    my_game.place(x=10,y=10)

    game_scroll.config(command=my_game.yview)
    game_scroll.config(command=my_game.xview)

    #define our column

    my_game['columns'] = ('Date', '3pts', '2pts', '1pts','Total')

    # format our column
    my_game.column("#0", width=0,  stretch=NO)
    my_game.column("Date",anchor=CENTER,width=80)
    my_game.column("3pts",anchor=CENTER,width=80)
    my_game.column("2pts",anchor=CENTER,width=80)
    my_game.column("1pts",anchor=CENTER,width=80)
    my_game.column("Total",anchor=CENTER,width=80)

    #Create Headings 
    my_game.heading("#0",text="",anchor=CENTER)
    my_game.heading("Date",text="Date",anchor=CENTER)
    my_game.heading("3pts",text="3pts",anchor=CENTER)
    my_game.heading("2pts",text="2pts",anchor=CENTER)
    my_game.heading("1pts",text="1pts",anchor=CENTER)
    my_game.heading("Total",text="Total",anchor=CENTER)
 

    for i in range(len(datasource)):
      #add data  
      my_game.insert(parent='',index='end',iid=i,text='',values=(datasource[i]))

    #end function 


#start code main

datasource = []


import tkinter as tk                     
from tkinter import ttk 
from functools import partial
from tkinter import * 


app = tk.Tk() 
app.title('basket ball')
tabControl = ttk.Notebook(app) 
  
tab1 = ttk.Frame(tabControl, style='My.TFrame') 
tab2 = ttk.Frame(tabControl) 

 
tabControl.add(tab1, text ='Main') 
tabControl.add(tab2, text ='DashBoard') 
tabControl.pack(expand = 1, fill ="both") 
  
############################# content in Main #######################

#add more button
buttona = tk.Button(tab1,text='  view  ', command=partial(calculator,'p'))
buttona.place(x=40,y=280) #replace values with required
#button.pack()  #comment this


buttona = tk.Button(tab1,text='  save  ', command=partial(save))
buttona.place(x=100,y=280) #replace values with required



label4 = tk.Label(tab1,text='1-pts',width=4,borderwidth=1)
label4.place(x=40,y=10)

textbox0 = tk.Entry(tab1)
textbox0.place(x=40,y=30)
textbox0.insert(tk.END,'0')
textbox0.config(font =("Courier", 14))


label1 = tk.Label(tab1,text='2-pts',width=4,borderwidth=1)
label1.place(x=40,y=50)

textbox1 = tk.Entry(tab1)
textbox1.place(x=40,y=70)
textbox1.insert(tk.END,'0')
textbox1.config(font =("Courier", 14))
#textbox.pack()  

label2 = tk.Label(tab1,text='3-pts',width=4,borderwidth=1)
label2.place(x=40,y=100)

textbox2 = tk.Entry(tab1)
textbox2.place(x=40,y=120)
textbox2.insert(tk.END,'0')
textbox2.config(font =("Courier", 14))


label3 = tk.Label(tab1,text='total score',width=8,borderwidth=1)
label3.place(x=40,y=150)

textboxans = tk.Entry(tab1)
textboxans.place(x=40,y=170)
textboxans.insert(tk.END,'0')
textboxans.config(font =("Courier", 14))


labelDate = tk.Label(tab1,text='Date',width=4,borderwidth=1)
labelDate.place(x=40,y=220)

textboxDate = tk.Entry(tab1)
textboxDate.place(x=40,y=240)
textboxDate.insert(tk.END,'')
textboxDate.config(font =("Courier", 14))


################################# end content in Main ################


############################# content in DashBoard #######################

buttona = tk.Button(tab2,text='  refresh  ', command=partial(refresh))
buttona.place(x=40,y=300) #replace values with required
 



################################# end content in DashBoard ################


app.resizable(0, 0)
app.geometry("500x500")
app.mainloop()