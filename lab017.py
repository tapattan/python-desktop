#listbox scrollbar
 
from tkinter import *
app = Tk()

listbox = Listbox(app)
for i in range(100):
    listbox.insert(END, i)
    
# attach listbox to scrollbar
scrollbar = Scrollbar(app,orient='vertical', command=listbox.yview)
 

listbox.grid(row=0,column=0, sticky='e')
scrollbar.grid(row=0,column=1, sticky='ns')

app.geometry("300x300")
app.mainloop()