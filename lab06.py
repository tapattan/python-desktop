def print_something(k):
    print('Hi..' +str(k))
    textbox.delete(0,tk.END)
    textbox.insert(tk.END,'Hi..'+str(k))


import tkinter as tk
from functools import partial

app = tk.Tk()
app.title('Hello world')

#เพิ่มปุ่มเข้ามา
button = tk.Button(app,text='click me!', command=partial(print_something,'David'))
button.place(x=0,y=0) #replace values with required
#button.pack()  #comment this

textbox = tk.Entry()
textbox.place(x=0,y=50)
textbox.config(font =("Courier", 14))
#textbox.pack()  

app.geometry("300x300")
app.mainloop()


