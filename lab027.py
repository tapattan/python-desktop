import tkinter as tk

def callWhenDestroy(event):
    print('close',event.widget)
    if(str(event.widget)=='.!toplevel'):
        print('****')
    global onPOPUP 
    onPOPUP = False
 
def create_window():
    global onPOPUP 
    if(onPOPUP==False):
        onPOPUP = True
        window = tk.Toplevel(root)
        window.geometry("300x300")
        window.title("New Window")
        tk.Label(window, text="Hello David!", font=('Helvetica 17 bold')).pack(pady=30)
        window.bind('<Destroy>', callWhenDestroy)

onPOPUP = False

root = tk.Tk()
b = tk.Button(root, text="Create new window", command=create_window)
b.pack()

root.geometry("300x300")
root.mainloop()