import tkinter as tk
from tkinter import filedialog as fd 

def callback():
    name= fd.askopenfilename() 
    print(name)

app = tk.Tk()
app.title('Hello world')

errmsg = 'Error!'
tk.Button(text='Click to Open File', 
       command=callback).grid(column=0,row=0)


app.geometry("300x300")
app.mainloop()